CS558 Homework 1.1 
Firstname Lastname
\begin{code}
module Matrices (rmvp, rmmp, rmt) where
  type Realvector = [Double]
  type Realmatrix = [[Double]]

  rmvp :: Realmatrix -> Realvector -> Realvector
  rmvp = undefined
  rmmp :: Realmatrix -> Realmatrix -> Realmatrix
  rmmp = undefined
  rmt :: Realmatrix -> Realmatrix
  rmt = undefined
\end{code}

-- This is a quicksort demo

quickSort :: (Ord a) => [a] -> [a]

quickSort [] = []
quickSort (x:xs) = 
  let small = quickSort [a | a <- xs, a <= x]
      big = quickSort [a | a <-xs, a > x]
  in small ++ [x] ++ big
              
             

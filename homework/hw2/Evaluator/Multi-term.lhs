\documentclass[11pt,a4paper]{article}
\usepackage{url}

% define \ignore{}
\long\def\ignore#1{}


%include lhs2TeX.fmt
%include lhs2TeX.sty

% redefine {code}
\usepackage{listings}
\usepackage{xcolor}
\lstloadlanguages{Haskell}
\lstnewenvironment{code}{
    {\lstset{}%
      \csname lst@SetFirstLabel\endcsname}
    {\csname lst@SaveFirstLabel\endcsname}
    \lstset{
      basicstyle=\small\ttfamily,
      frame=single,
      flexiblecolumns=false,
      basewidth={0.5em,0.45em},
      literate={+}{{$+$}}1 {/}{{$/$}}1 {*}{{$*$}}1 {=}{{$=$}}1
               {>}{{$>$}}1 {<}{{$<$}}1 {\\}{{$\lambda$}}1
               {\\\\}{{\char`\\\char`\\}}1
               {->}{{$\rightarrow$}}2 {>=}{{$\geq$}}2 {<-}{{$\leftarrow$}}2
               {<=}{{$\leq$}}2 {=>}{{$\Rightarrow$}}2 
               {\ .}{{$\circ$}}2 {\ .\ }{{$\circ$}}2
               {>>}{{>>}}2 {>>=}{{>>=}}2
    }}{}

% no indention
% \setlength{\parindent}{0in}

\begin{document}
\title{CS558: Homework 2}
\author{Zhenjie Chen}
\date{\today}

\maketitle

% show (Succ t) | isNumericValue t = showAsNum t 1
%               | otherwise = "(succ " ++ show t ++ ")"
%               where showAsNum Zero num = show num
%                     showAsNum (Succ t) num = showAsNum t (num + 1)

\begin{code}
module Main where
import qualified System.Environment
import qualified Data.Map as M
import Text.Regex(subRegex, mkRegex)

data Term = Tru
          | Fls
          | If Term Term Term
          | Zero
          | Succ Term
          | Pred Term
          | IsZero Term
          deriving Eq

instance Show Term where
  show Tru = "true"
  show Fls = "false"
  show (If c t1 t2) = "if " ++ show c ++ " then " ++ show t1 ++ " else " ++ show t2 ++ " fi"
  show Zero = "0"
  show (Succ t) = "succ(" ++ show t ++ ")"
  show (Pred t) = "pred(" ++ show t ++ ")"
  show (IsZero t) = "iszero(" ++ show t ++ ")"
  
 
-- testing whether it is Tru, Fls or numeric value
isValue :: Term -> Bool
isValue Tru = True
isValue Fls = True
isValue t  = isNumericValue t
                                         
-- testing whether it is numeric value
isNumericValue :: Term -> Bool
isNumericValue Zero = True
isNumericValue (Succ t) = isNumericValue t
isNumericValue (Pred t) = isNumericValue t
isNumericValue _ = False

--one step
eval1 :: Term -> Maybe Term
eval1 (If Tru t2 _) = Just t2
eval1 (If Fls _ t3) = Just t3
eval1 (If t1 t2 t3) =  
  case eval1 t1 of
    Nothing -> Nothing
    Just Tru -> Just t2
    Just Fls -> Just t3 
    Just t1' -> Just (If t1' t2 t3)
eval1 (Succ t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Succ t1)
eval1 (Pred Zero) = Just Zero
eval1 (Pred (Succ nv)) 
  | isNumericValue nv = Just nv
eval1 (Pred t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Pred t1)
eval1 (IsZero Zero) = Just Tru
eval1 (IsZero (Succ nv))
  | isNumericValue nv = Just Fls
eval1 (IsZero t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(IsZero t1)
eval1 _ = Nothing

  
--multi-step (simple, non-tracing evaluator:
-- it is a good idea to write this first)
eval :: Term -> Term
eval t = 
  case eval1 t of
    Nothing -> t
    Just t1 -> eval t1
  

data Token = TrueKeyword
           | FalseKeyword
           | IfKeyword
           | ThenKeyword
           | ElseKeyword
           | FiKeyword
           | ZeroKeyword
           | SuccKeyword
           | PredKeyword
           | IsZeroKeyword
           | LPar
           | RPar
           | Semicolon
           deriving (Eq, Show)


scan :: String -> [Token]
scan s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] 
      | token `elem` keywordsList -> hash M.! token : scan tl
      | otherwise -> error $ "\nInvalid token: '" ++ token ++ "'"
      where 
        keywordsList = ["true", "false", "if", "then", "else", "fi", "0", "succ", "(", ")", "pred", "iszero", ";"]
        tokenList = [TrueKeyword, FalseKeyword, IfKeyword, ThenKeyword, ElseKeyword, FiKeyword, ZeroKeyword, SuccKeyword, LPar, RPar, PredKeyword, IsZeroKeyword, Semicolon]
        hash = M.fromList $ zip keywordsList tokenList


parseTerm :: [Token] -> Maybe (Term, [Token])
parseTerm (TrueKeyword:tl) = Just (Tru, tl)
parseTerm (FalseKeyword:tl) = Just (Fls, tl)
parseTerm (ZeroKeyword:tl) = Just(Zero, tl)
parseTerm (PredKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Pred t, tl1)
    _ -> error "Syntax error after 'pred'"
parseTerm (SuccKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Succ t, tl1)
    _ -> error "Syntax error after 'succ'"
parseTerm (IfKeyword:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t1, ThenKeyword:tl1) -> 
      case parseTerm tl1 of
        Nothing -> Nothing
        Just(t2, ElseKeyword:tl2) ->
          case parseTerm tl2 of
            Nothing -> Nothing
            Just(t3, FiKeyword:tl3) -> Just(If t1 t2 t3, tl3)
            _ -> error "Syntax error after 'else'"
    _ -> error "Syntax error after 'if'"
parseTerm (IsZeroKeyword:LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(IsZero t, tl1)
    _ -> error "Syntax error after 'iszero'"
parseTerm (LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just (t, tl1)
    _ -> error "Mismatching '(' and ')'"
parseTerm (Semicolon:tl1) = parseTerm tl1
parseTerm _ = Nothing


parse :: [Token] -> [Term]
parse t =
  case parseTerm t of
    Nothing -> []
    Just (term, []) -> [term]
    Just (term, Semicolon:tl) -> term : parse tl
        
printEval1 :: Term -> Maybe (String, Term)
printEval1 (If Tru t2 t3) = Just ("*" ++ show (If Tru t2 t3) ++ "*", t2)
printEval1 (If Fls t2 t3) = Just ("*" ++ show (If Fls t2 t3) ++ "*", t3)
printEval1 (If t1 t2 t3) = 
  case printEval1 t1 of 
    Nothing -> Nothing -- correct
    Just (str, term) -> Just("if " ++ str ++ " then " ++ show t2 ++ " else " ++ show t3 ++ " fi", If term t2 t3)
printEval1 (Succ t1) =
  case printEval1 t1 of
    Nothing -> Nothing
    Just (str, term) -> Just("succ(" ++ str ++ ")", Succ term)
printEval1 (Pred Zero) = Just("*" ++ show (Pred Zero) ++ "*", Zero)
printEval1 (Pred (Succ nv)) =
  case isNumericValue nv of
    True -> Just("*"++ show (Pred (Succ nv)) ++ "*", nv)
    False -> Nothing
printEval1 (Pred t1) = 
  case printEval1 t1 of
    Nothing -> Nothing
    Just (str, term) -> Just("pred(" ++ str ++")", Pred term)
printEval1 (IsZero Zero) = Just("*iszero(0)*", Tru)
printEval1 (IsZero (Succ nv)) = 
  case isNumericValue nv of
    True -> Just("*" ++ show (IsZero (Succ nv)) ++ "*", Fls)
    False -> Nothing
printEval1 (IsZero t1) = 
  case printEval1 t1 of
    Nothing -> Nothing
    Just(str, term) -> Just("iszero(" ++ str ++ ")", IsZero term)
printEval1 _ = Nothing

printEval :: Term -> [String]
printEval t =
  case printEval1 t of
    Just (s, t') -> s:printEval t'
    Nothing -> []

traceEval :: Term -> IO ()
traceEval t = sequence_ (map putStrLn (printEval t))

main :: IO ()
main =
  do
    args <- System.Environment.getArgs
    let [sourceFile] = args
    source <- readFile sourceFile
    let tokens = scan $ subRegex (mkRegex "--.*\n") source ""
    let terms = parse tokens
    putStrLn ("---Trace:---")
    sequence_ (map traceEval terms)
    putStrLn ("---Normal form:---")
    putStrLn (show (map eval terms))

\end{code}

\section{Miscellaneous}
\subsection{Compile}
\begin{itemize}
\item Build all\\
  make all
\item Build evaluator\\
  make evaluator\\
\item Build doc\\
  make doc
\item Clean\\
  make clean
\end{itemize}
        
\subsection{Run test}
This is the test!

\end{document}
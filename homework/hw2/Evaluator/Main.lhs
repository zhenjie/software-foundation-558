\documentclass[11pt,a4paper]{article}
\usepackage{url}

\usepackage{times}
\usepackage{mathptmx}

% define \ignore{}
\long\def\ignore#1{}


%include lhs2TeX.fmt
%include lhs2TeX.sty

% redefine {code}
\usepackage{listings}
\usepackage{xcolor}
\lstloadlanguages{Haskell}
\lstnewenvironment{code}{
    {\lstset{}%
      \csname lst@SetFirstLabel\endcsname}
    {\csname lst@SaveFirstLabel\endcsname}
    \lstset{
      basicstyle=\small\ttfamily,
      frame=single,
      flexiblecolumns=false,
      basewidth={0.5em,0.45em},
      literate={+}{{$+$}}1 {/}{{$/$}}1 {*}{{$*$}}1 {=}{{$=$}}1
               {>}{{$>$}}1 {<}{{$<$}}1 {\\}{{$\lambda$}}1
               {\\\\}{{\char`\\\char`\\}}1
               {->}{{$\rightarrow$}}2 {>=}{{$\geq$}}2 {<-}{{$\leftarrow$}}2
               {<=}{{$\leq$}}2 {=>}{{$\Rightarrow$}}2 
               {\ .}{{$\circ$}}2 {\ .\ }{{$\circ$}}2
               {>>}{{>>}}2 {>>=}{{>>=}}2
    }}{}

% \lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},frame=single, language=haskell, numbers=left, numberstyle=\tiny\color{gray},basicstyle={\scriptsize}}
\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},language=haskell, basicstyle={\scriptsize}}

% no indention
% \setlength{\parindent}{0in}

\begin{document}
\title{CS558: Homework 2}
\author{Zhenjie Chen}
\date{\today}

\maketitle


\section{Introduction}
The formal syntax of a term is:
\begin{lstlisting}
  term ::= true  
           false
           if Term then Term else Term fi
           0
           succ lpar Term rpar
           pred lpar Term rpar
           iszero lpar term rpar
           lpar Term rpar
\end{lstlisting}
We need to define our own data structure to represent {\bf term}, here is the {\it Term} we
defined,
\begin{code}
module Main where
import qualified System.Environment
import qualified Data.Map as M
import Text.Regex(subRegex, mkRegex)

data Term = Tru
          | Fls
          | If Term Term Term
          | Zero
          | Succ Term
          | Pred Term
          | IsZero Term
          deriving Eq
  
\end{code}

We also need to define the {\it show} function which might be called in trace and evaluation 
process. Every {\it term} has its own show function,
\begin{code}
instance Show Term where
  show Tru = "true"
  show Fls = "false"
  show (If c t1 t2) = 
    "if " ++ show c ++ " then " ++ show t1 ++ " else " ++ show t2 ++ " fi"
  show Zero = "0"
  show (Succ t) = "succ(" ++ show t ++ ")"
  show (Pred t) = "pred(" ++ show t ++ ")"
  show (IsZero t) = "iszero(" ++ show t ++ ")"
\end{code}
\section{Design}

\subsection{Scanner}
There exist several ways to implement the scanner. One of the most easiest way is using
pattern matching to match all the cases. Easy as it is, it might provide more information 
on the lexical error. Here is the code snippet for matching 'if':
\begin{lstlisting}
  con s = s == [] || isSpace (s!!0) || (s!!0) == '(' || (s!!0) == ')'
  scan ('i':'f':s) = 
     case con s of
       True ->  IfKeyword : scan s
       False ->  error ``Invalid token''
\end{lstlisting}
The other way to implement scanner is using {\it splitRegex} or {\it words} functions to split
the input string into a list of tokens. 

Haskell provide another handy function named {\it lex :: ReadS String} which returns 
first token of the input string. Here is the scanner I wrote for this exercise,
\begin{code}
scan :: String -> [Token]
scan s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] 
      | token `elem` keywordsList -> hash  M.! token : scan tl
      | otherwise -> error $ "\nInvalid token: '" ++ token ++ "'"
      where 
        keywordsList = 
          ["true", "false", "if", "then", "else", "fi", "0", "succ", "(", ")", 
           "pred", "iszero"]
        tokenList = 
          [TrueKeyword, FalseKeyword, IfKeyword, ThenKeyword, ElseKeyword, 
           FiKeyword, ZeroKeyword, SuccKeyword, LPar, RPar, PredKeyword, 
           IsZeroKeyword]
        hash = M.fromList $ zip keywordsList tokenList
\end{code}
While the data type {\it Token} is defined as following,
\begin{code}
data Token = TrueKeyword
           | FalseKeyword
           | IfKeyword
           | ThenKeyword
           | ElseKeyword
           | FiKeyword
           | ZeroKeyword
           | SuccKeyword
           | PredKeyword
           | IsZeroKeyword
           | LPar
           | RPar
           deriving (Eq, Show)
\end{code}

If any invalid token occurs, the scanner would simply print the invalid token and abort 
the program. Invalid token or characters could be {\it ``\#'', ``iff'', ``isZero'', etc}.

\subsection{Parser\label{sec:parser}}
The parser will parse the list of tokens we just get from scanner and produce a {\bf term}.
To parse the tokens, we will follow the formal syntax and build the term from {\bf sub term} 
recursively and drop tokens(keywords) like {\it ``fi'', ``else'', ``then'', ``(``, ``)''}.
Simple error message would be returned when this parser encounters any problem. The 
message does not include where the error is located in the source code though.
Here is the definition of {\it parseTerm} function,
\begin{code}
parseTerm :: [Token] -> Maybe (Term, [Token])
parseTerm (TrueKeyword:tl) = Just (Tru, tl)
parseTerm (FalseKeyword:tl) = Just (Fls, tl)
parseTerm (ZeroKeyword:tl) = Just(Zero, tl)
parseTerm (PredKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Pred t, tl1)
    _ -> error "Syntax error after 'pred'"
parseTerm (SuccKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Succ t, tl1)
    _ -> error "Syntax error after 'succ'"
parseTerm (IfKeyword:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t1, ThenKeyword:tl1) -> 
      case parseTerm tl1 of
        Nothing -> Nothing
        Just(t2, ElseKeyword:tl2) ->
          case parseTerm tl2 of
            Nothing -> Nothing
            Just(t3, FiKeyword:tl3) -> Just(If t1 t2 t3, tl3)
            _ -> error "Syntax error after 'else'"
    _ -> error "Syntax error after 'if'"
parseTerm (IsZeroKeyword:LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(IsZero t, tl1)
    _ -> error "Syntax error after 'iszero'"
parseTerm (LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just (t, tl1)
    _ -> error "Mismatching '(' and ')'"
parseTerm _ = Nothing
\end{code}

The {\it parse} function will call {\it parseTerm} and accept the {\bf term} produced.
By changing this function we can make our program to support {\bf multi-terms} which 
separated by semicolon.(Section \ref{sec:multi-terms}) 
\begin{code}
parse :: [Token] -> Term
parse t =
  case parseTerm t of
    Nothing -> error "Syntax error or empty file"
    Just (term, []) -> term
\end{code}

\subsection{Evaluator}
To evaluate a {\bf term}, we have to walk the syntax tree of the {\bf term} and evaluate each {\bf sub-term}.
Semantic error could be found in this process while ignored in this exercise.(Section: \ref{subsec:type-error})

Inspired by the ML code on page 47-48 of textbook, we can get the one step evaluation function {\it eval1} as following,
\begin{code}
eval1 :: Term -> Maybe Term
eval1 (If Tru t2 _) = Just t2
eval1 (If Fls _ t3) = Just t3
eval1 (If t1 t2 t3) =  
  case eval1 t1 of
    Nothing -> Nothing
    Just Tru -> Just t2
    Just Fls -> Just t3 
    Just t1' -> Just (If t1' t2 t3)
eval1 (Succ t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Succ t1)
eval1 (Pred Zero) = Just Zero
eval1 (Pred (Succ nv)) 
  | isNumericValue nv = Just nv
eval1 (Pred t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Pred t1)
eval1 (IsZero Zero) = Just Tru
eval1 (IsZero (Succ nv))
  | isNumericValue nv = Just Fls
eval1 (IsZero t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(IsZero t1)
eval1 _ = Nothing
\end{code}

Here is the control function {\it eval} which recursively calls {\it eval1} until reaching the normal form,
\begin{code}
eval :: Term -> Term
eval t = 
  case eval1 t of
    Nothing -> t
    Just t1 -> eval t1
\end{code}
  
\subsection{Tracer}
The tracer likes evaluator except that it will also return a rewritten {\bf sub term} as string.
Therefore, we have to carefully construct the string in the way of evaluation. For the successor 
operation, we can simply print out the original form as we will do in this exercise, like {\it succ(0)}.
Alternatively we can print out the real value of {\it succ(0)} which is $1$. This requires some change 
in the {\it show} function for {\it Succ t}. 
\begin{lstlisting}
% show (Succ t) | isNumericValue t = showAsNum t 1
%               | otherwise = "succ( " ++ show t ++ ")"
%               where showAsNum Zero num = show num
%                     showAsNum (Succ t) num = showAsNum t (num + 1)
\end{lstlisting}

Here is the function {\it printEval1} which highlights the redex on each step, 
\begin{code}
printEval1 :: Term -> Maybe (String, Term)
printEval1 (If Tru t2 t3) = Just ("*" ++ show (If Tru t2 t3) ++ "*", t2)
printEval1 (If Fls t2 t3) = Just ("*" ++ show (If Fls t2 t3) ++ "*", t3)
printEval1 (If t1 t2 t3) = 
  case printEval1 t1 of 
    Nothing -> Nothing -- correct
    Just (str, term) -> 
      Just("if " ++ str ++ " then " ++ show t2 ++ " else " ++  show t3 
           ++ " fi", If term t2 t3)
printEval1 (Succ t1) =
  case printEval1 t1 of
    Nothing -> Nothing
    Just (str, term) -> Just("succ(" ++ str ++ ")", Succ term)
printEval1 (Pred Zero) = Just("*" ++ show (Pred Zero) ++ "*", Zero)
printEval1 (Pred (Succ nv)) =
  case isNumericValue nv of
    True -> Just("*"++ show (Pred (Succ nv)) ++ "*", nv)
    False -> Nothing
printEval1 (Pred t1) = 
  case printEval1 t1 of
    Nothing -> Nothing
    Just (str, term) -> Just("pred(" ++ str ++")", Pred term)
printEval1 (IsZero Zero) = Just("*iszero(0)*", Tru)
printEval1 (IsZero (Succ nv)) = 
  case isNumericValue nv of
    True -> Just("*" ++ show (IsZero (Succ nv)) ++ "*", Fls)
    False -> Nothing
printEval1 (IsZero t1) = 
  case printEval1 t1 of
    Nothing -> Nothing
    Just(str, term) -> Just("iszero(" ++ str ++ ")", IsZero term)
printEval1 _ = Nothing
\end{code}

Again, control functions,
\begin{code}
printEval :: Term -> [String]
printEval t =
  case printEval1 t of
    Just (s, t') -> s:printEval t'
    Nothing -> []

traceEval :: Term -> IO ()
traceEval t = sequence_ (map putStrLn (printEval t))
\end{code}

\section{Error detection}
\subsection{Syntax error}
I didn't do much on the syntax error detection in this exercise. It can only detect simple 
error such as {\it mismatch parenthesis, lack of keywords, unnecessary keywords, etc}. and 
return message ``Syntax error'' for all other unrecognized syntax error. The actual
code is listed in Section \ref{sec:parser}.

\subsection{Type error\label{subsec:type-error}}
Type checking is out of the scope of this exercise, thus we don't care about the type error in
code like\footnote{See testcase example7.BN}:
\begin{lstlisting}
  if true then succ(0) else false fi
\end{lstlisting}

\section{Multi-terms\label{sec:multi-terms}}
To ease the testing, I extend the {\bf term} to {\bf terms} which contains several 
{\bf term} separated by semicolon. 
First adding a new token keyword, say {\bf Semicolon} and make some changes to 
scanner, parser as well as the Main function. These can be found in file {\it Multi-term.lhs} 
which will not be included in this report. I also add support for writing single comment, 
like that in haskell, in the terms' source file.

\section{Miscellaneous}
\subsection{Main and helper}
Testing whether a {\bf term} is value or not, $true, false, 0$,
\begin{code}
isValue :: Term -> Bool
isValue Tru = True
isValue Fls = True
isValue t  = isNumericValue t
\end{code}

Testing whether a {\bf term} is numerical or not, $0, succ(0), pred(0), etc$,
\begin{code}
isNumericValue :: Term -> Bool
isNumericValue Zero = True
isNumericValue (Succ t) = isNumericValue t
isNumericValue (Pred t) = isNumericValue t
isNumericValue _ = False
\end{code}

Main function which control the whole program flow,
\begin{code}
main :: IO ()
main =
  do
    args <- System.Environment.getArgs
    let [sourceFile] = args
    source <- readFile sourceFile
    let tokens = scan $ subRegex (mkRegex "--.*\n") source ""
    let term = parse tokens
    putStrLn ("---Trace:---")
    traceEval term
    putStrLn ("---Normal form:---")
    putStrLn (show (eval term))

\end{code}

\subsection{Compile}
Refert to Makefile for more information,
\begin{enumerate}
\item {\it make bn} will build the executable file {\bf bn};
\item {\it make mbn} will build the executable file {\bf mbn} for multi-terms' testing;
\item {\it make all} will build the executable file {\bf bn}, document {\bf homework2.pdf} and open it with {\bf evince};
\item {\it make clean} clean  the project.
\end{enumerate}

\subsection{Testing}
Thanks to the support of multi-term, we can run the test in one time. Using the following commands,
\begin{lstlisting}
  make mbn
  ./mbn ../Testcase/example7.BN
\end{lstlisting}

For all the 7 examples, example 1, 2 is them same as in the exercise while example 3 to 6 are all codes with syntax error. Example 7 contains most of the basic cases for {\bf term}. I provide a simple script {\it runtest} which will run all these tests, and {\it runtest arg1 arg2} would run test for example arg1 and arg2.

\subsection{Test log}
\subsubsection{Example 1} 
{\it ./bn ../Testcase/example1.BN}
\begin{lstlisting}
---Trace:---
if *iszero(0)* then 0 else pred(0) fi
*if true then 0 else pred(0) fi*
---Normal form:---
0  
\end{lstlisting}

\subsubsection{Example 2}
{\it ./bn ../Testcase/example2.BN}
\begin{lstlisting}
---Trace:---
---Normal form:---
if iszero(pred(false)) then if true then pred(0) else succ(false) fi else succ(0) fi
\end{lstlisting}

\subsubsection{Example 3}
{\it ./bn ../Testcase/example3.BN}
\begin{lstlisting}
  ---Trace:---
  bn: Syntax error after 'iszero'
\end{lstlisting}

\subsubsection{Example 4}
{\it ./bn ../Testcase/example4.BN}
\begin{lstlisting}
---Trace:---
bn: 
Invalid token: 'isZero'
\end{lstlisting}

\subsubsection{Example 5}
{\it ./bn ../Testcase/example5.BN}
\begin{lstlisting}
---Trace:---
bn: Mismatching '(' and ')'
\end{lstlisting}

\subsubsection{Example 6}
{\it ./bn ../Testcase/example6.BN}
\begin{lstlisting}
---Trace:---
bn: Syntax error after 'if'
\end{lstlisting}

\subsubsection{More complicate case}
{\it ./mbn ../Testcase/example7.BN\footnote{See appendix}}.
\begin{lstlisting}
./mbn ../Testcase/example7.BN 
---Trace:---
*pred(0)*
*iszero(0)*
*pred(succ(0))*
succ(*pred(0)*)
pred(*pred(0)*)
*pred(0)*
*iszero(succ(0))*
iszero(*pred(0)*)
*iszero(0)*
*iszero(succ(pred(0)))*
iszero(*pred(succ(0))*)
*iszero(0)*
*if true then true else false fi*
if iszero(*pred(0)*) then succ(0) else pred(0) fi
if *iszero(0)* then succ(0) else pred(0) fi
*if true then succ(0) else pred(0) fi*
if *iszero(succ(0))* then if iszero(0) then pred(0) else pred(succ(0)) fi else \
    if iszero(succ(pred(pred(pred(pred(pred(0))))))) then false else if true then\
    succ(0) else pred(0) fi fi fi
*if false then if iszero(0) then pred(0) else pred(succ(0)) fi else if \
    iszero(succ(pred(pred(pred(pred(pred(0))))))) then false else if true \
    then succ(0) else pred(0) fi fi fi*
if *iszero(succ(pred(pred(pred(pred(pred(0)))))))* then false else if true\
    then succ(0) else pred(0) fi fi
*if false then false else if true then succ(0) else pred(0) fi fi*
*if true then succ(0) else pred(0) fi*
if iszero(*pred(0)*) then if iszero(0) then pred(0) else pred(succ(0)) fi \
    else if iszero(succ(pred(pred(pred(pred(pred(0))))))) then false else if\
    true then succ(0) else pred(0) fi fi fi
if *iszero(0)* then if iszero(0) then pred(0) else pred(succ(0)) fi else if \
    iszero(succ(pred(pred(pred(pred(pred(0))))))) then false else if true \
    then succ(0) else pred(0) fi fi fi
*if true then if iszero(0) then pred(0) else pred(succ(0)) fi else if \
    iszero(succ(pred(pred(pred(pred(pred(0))))))) then false else if true \
    then succ(0) else pred(0) fi fi fi*
if *iszero(0)* then pred(0) else pred(succ(0)) fi
*if true then pred(0) else pred(succ(0)) fi*
*pred(0)*
*if true then succ(0) else false fi*
---Normal form:---
[true,false,0,succ(0),0,true,0,succ(0),succ(succ(0)),0,false,true,false,true,\
    true,succ(0),succ(0),0,succ(0)]  
\end{lstlisting}

\appendix 
\section{example7.BN}
\begin{lstlisting}
-- basic cases, single
true;
false;
0;
succ(0); -- succ(0)
pred(0); -- 0
iszero(0); -- true

-- combination
pred(succ(0)); -- 0
succ(pred(0)); -- succ(0)
succ(succ(0)); -- succ(succ(0))
pred(pred(0)); -- 0

iszero(succ(0)); -- false
iszero(pred(0)); -- true
iszero(succ(pred(0))); -- false
iszero(pred(succ(0))); -- true

if true 
then 
    true 
else 
    false 
fi; -- true

if iszero(pred(0)) 
then
    succ(0)
else
    pred(0)
fi; -- succ(0)

-- more complicate
if iszero(succ(0))
then
    if iszero(0)
    then
        pred(0)
    else
        pred(succ(0))
    fi
else
    if iszero(succ(pred(pred(pred(pred(pred(0)))))))
    then 
        false
    else
        if true
        then
            succ(0)
        else
            pred(0)
        fi
    fi
fi; -- succ(0)

if iszero(pred(0))
then
    if iszero(0)
    then
        pred(0)
    else
        pred(succ(0))
    fi
else
    if iszero(succ(pred(pred(pred(pred(pred(0)))))))
    then 
        false
    else
        if true
        then
            succ(0)
        else
            pred(0)
        fi
    fi
fi; -- 0
        
-- other cases
if true then succ(0) else false fi;  
\end{lstlisting}
\end{document}

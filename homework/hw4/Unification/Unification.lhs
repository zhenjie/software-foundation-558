\documentclass[11pt]{article}
%% Literate Haskell script intended for lhs2TeX.

%include polycode.fmt
%format `union` = "\cup"
%format alpha = "\alpha"
%format gamma = "\gamma"
%format capGamma = "\Gamma"
%format tau = "\tau"
%format tau1 = "\tau_{1}"
%format tau2 = "\tau_{2}"
%format tau11 = "\tau_{11}"
%format tau12 = "\tau_{12}"
%format t12 = "t_{12}"
%format t1 = "t_{1}"
%format t1' = "t_{1}^{\prime}"
%format t2 = "t_{2}"
%format t2' = "t_{2}^{\prime}"
%format t3 = "t_{3}"
%format nv1 = "nv_{1}"

\usepackage{listings}
\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},language=haskell, basicstyle={\scriptsize}}

\usepackage{enumerate}
\usepackage{xcolor}

\usepackage{fullpage}
\usepackage{mathpazo}
\usepackage{graphicx}
\usepackage{color}
\usepackage[centertags]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{soul}

\usepackage{stmaryrd}

\title{General-purpose unfication\footnote{CS558: Homework 4(2)}}
\author{Zhenjie Chen}
\date{\today}

\begin{document}
\thispagestyle{empty}
\maketitle

% \newpage

\section{Introduction}
In this paper, we will implement the nondeterministic algorithm given by Martelli and Montanari(1982)
using Haskell. The algorithm is based on two transformations: {\it Term Reduction and Variable Elimination}. 
To implement the general-purpose unification program, we design data structures for terms(data Term v f) and for 
unifier(Equation v f). We also define a {\it solved formed} checker(sf) to determine when we can return the 
unifier, which turns out to be a {\it most general unifier (mgu)}. Substitution functions, applySubst' and applySubst, 
are given to perform the substitution on the equations. Last but not least, the unify function is defined
to do the real unification job, based on the rules in Martelli and Montanari(1982). Five test-cases will be
provided at the end of this paper.

\section{Design and implementation}
\subsection{Data structures}

\begin{code}
module Main where

import qualified Data.List

data Term v f = Fun f [Term v f]
              | Var v

instance (Show v, Show f) => Show (Term v f) where
    show (Fun f ts) = clear(show f) ++ "(" ++ showTs ts ++ ")"
         where showTs [] = ""
               showTs (t:[]) = show t
               showTs (t:ts) = (show t) ++ ", " ++ showTs ts
    show (Var v) = clear(show v)

type Equation v f = (Term v f, Term v f)
type Binding v f = (v, Term v f)
type Substitution v f = [Binding v f]

data EquationOutcome = HaltWithFailure | HaltWithCycle | NoMatch | Success 
     deriving(Show, Eq)
\end{code}

\subsection{Substitution functions}
\begin{code}
applySubst :: (Eq v, Eq f) => Term v f -> Substitution v f -> Term v f
applySubst (Var x) theta =
           case Data.List.find (\(z,_)-> z == x) theta of
                Nothing -> Var x
                Just (_,t) -> t
applySubst (Fun f tlist) theta =
           Fun f (map (\t-> applySubst t theta) tlist)


applySubst' :: (Eq v, Eq f) => [Equation v f] -> Substitution v f -> [Equation v f]
applySubst' eql theta = map (\(s,t)-> (applySubst s theta, applySubst t theta)) eql
\end{code}


\subsection{Helper function}
To get rid of the additional '"' for function name, there should be 
a better solution for this.
\begin{code}
clear :: String -> String
clear ('"':s) | last s == '"' = init s
              | otherwise = s
clear s       = s 
\end{code}


\subsection{Solved form checker}
As in the paper, a set of equations is said to be in {\it solved formed} iff
it satisfies the following conditions:
\begin{enumerate}[(1)]
    \item the equations are $x_j = t_j, j = 1, ..., k$;
    \item every variable which is the left member of some equation occurs only there.
\end{enumerate}
The following code is simply based on these two rules.
\begin{code}
occur :: (Eq v, Eq f) => v -> Term v f -> Bool
occur x (Var y) = x == y
occur x (Fun f terms) = any (occur x) terms

sf :: (Eq v, Eq f) => [Equation v f] -> [Term v f] -> Bool
sf [] _ = True
sf ((Fun _ _, _):_) _ = False
sf ((Var x, t):eql) ts = 
         (not ( any (occur x) (t:ts) || any (\(t1, t2)-> occur x t1 || occur x t2) eql))
                  && sf eql (t:ts)
\end{code}


\subsection{Unification algorithm}
Based on the {\it algorithm 1} in the paper(1982).  Given a set of equations, repeatedly perform any of the following transformations. 
If no transformation applies, stop with success.
\begin{enumerate}[(a)]
    \item Select any equation of the form $$t=x$$ where $t$ is not a variable and $x$ is a variable, and rewrite it as $$x=t.$$
    \item Select any equation of the form $$x=x$$ where $x$ is variable, and erase it.
    \item Select any equation of the form $$t'=t''$$ where $t'$ and $t''$ are not variables. If the two root function symbols are different, stop 
          with failure; otherwise, apply term reduction.
    \item Select any equation of the form $$x=t$$ where $x$ is a variable which occurs somewhere else in the set of equations and where $t not = x$.
          If $x$ occurs in $t$, then stop with failure; otherwise, apply variable elimination.
\end{enumerate}

\begin{code}
unify :: (Eq v, Eq f) => [Equation v f] -> (EquationOutcome, [Equation v f])
unify eqs
      | sf eqs [] = (Success, eqs)
      | otherwise = 
         case eqs of 
           ((Fun f1 terms1, Var y):eql) -> unify $ (Var y, Fun f1 terms1):eql
           (eq@(Var x, t):eql) ->
              case t of 
                Var y ->
                   if x == y then unify eql
                   else unify $ applySubst' eql [(x, t)] ++ [eq]
                Fun f _ -> 
                   if occur x t then (HaltWithCycle, [])
                   else unify $ applySubst' eql [(x, t)] ++ [eq]
           ((Fun f1 terms1, Fun f2 terms2):eql) ->
              if f1 == f2 then unify $ zip terms1 terms2 ++ eql
              else (NoMatch, [])
           _ -> (HaltWithFailure, [])
\end{code}

\section{Compile}
Refert to Makefile for more information,
\begin{enumerate}
\item {\it make unification} will build the executable file {\bf unification};
\item {\it make doc} will build the document {\bf Unification.pdf}
\item {\it make all} will build the executable file {\bf unification}, document {\bf Unification.pdf} and open it with {\bf READER};
\item {\it make clean} clean  the project.
\end{enumerate}


\section{Testing}
\subsection{Test-cases}
\begin{code}
s1 = Fun "f" [Fun "g" [Fun "a" [], Var "X"], Fun "h" [Fun "f" [Var "Y", Var "Z"]]]
s2 = Fun "g" [Var "Y", Fun "h" [Fun "f" [Var "Z", Var "U"]]]
t1 = Fun "f" [Var "U", Fun "h" [Fun "f" [Var "X", Var "X"]]]
t2 = Fun "g" [Fun "f" [Fun "h" [Var "X"], Fun "a" []], Fun "h" [Fun "f" [Fun "a" [], Fun "b" []]]]
problem0 = [(s1, t1), (s2, t2)]

s3 = Fun "f" [Var "X1", Fun "h" [Var "X1"], Var "X2"]
t3 = Fun "f" [Fun "g" [Var "X3"], Var "X4", Var "X3"]
s4 = Fun "g" [Var "X2"]
t4 = Var "X1"
problem1 = [(s3, t3), (s4, t4)]

s5 = Fun "p" [Fun "foo" [Var "X"], Var "Y"]
t5 = Fun "p" [Var "a", Var "b"]
problem2 =  [(s5, t5)]

s6 = Fun "p" [Var "X", Fun "g" [Var "Z"], Var "X"]
t6 = Fun "p" [Fun "f" [Var "Y"], Var "Y", Var "W"]
problem3 =  [(s6, t6)]

s7 = Var "X"
t7 = Fun "f" [Var "Y"]
s8 = Var "Y"
t8 = Fun "f" [Fun "g" [Var "Z"]]
problem4 =  [(s7, t7), (s8, t8)]
\end{code}

\subsection{Helper}
These functions are used to format the solution output.
\begin{code}
showEq :: (Show v, Show f) => [Equation v f] -> String
showEq [] = ""
showEq ((t1, t2):[]) = "\t" ++ show t1 ++ " = " ++ show t2
showEq ((t1, t2):el) = "\t" ++ show t1 ++ " = " ++ show t2 ++ "\n" ++ showEq el

printProblem :: (Show v, Show f) => [Equation v f] -> String
printProblem eqs  = "Equations:\n" ++ showEq eqs

printSolution :: (Show v, Show f) => (EquationOutcome, [Equation v f]) -> String
printSolution (st, eq) 
              | st == Success = "EquationOutcome: " ++ (show st) ++ "\nSubstitutions:\n" ++ (showEq eq)
              | otherwise = "EquationOutcome: " ++ (show st) ++ "\nSolution: Nil"

puts :: (Eq v, Eq f, Show v, Show f) => [[Equation v f]] -> String
puts [] = ""
puts (p:[]) = printProblem p ++ "\n" ++ printSolution (unify p)
puts (p:ps) = printProblem p ++ "\n" ++ printSolution (unify p) ++ "\n\n" 
     ++ puts ps
            
main :: IO()
main =
     do 
        putStrLn $  puts [problem0, problem1, problem2, problem3, problem4]
\end{code}

\subsection{Test log}
Here is the log for running {\it ./runtest}, including all test-cases.
\begin{lstlisting}
bash-3.2$ ./unification
Equations:
	f(g(a(), X), h(f(Y, Z))) = f(U, h(f(X, X)))
	g(Y, h(f(Z, U))) = g(f(h(X), a()), h(f(a(), b())))
EquationOutcome: HaltWithCycle
Solution: Nil

Equations:
	f(X1, h(X1), X2) = f(g(X3), X4, X3)
	g(X2) = X1
EquationOutcome: Success
Substitutions:
	X1 = g(X3)
	X4 = h(g(X3))
	X2 = X3

Equations:
	p(foo(X), Y) = p(a, b)
EquationOutcome: Success
Substitutions:
	a = foo(X)
	Y = b

Equations:
	p(X, g(Z), X) = p(f(Y), Y, W)
EquationOutcome: Success
Substitutions:
	W = f(g(Z))
	X = f(g(Z))
	Y = g(Z)

Equations:
	X = f(Y)
	Y = f(g(Z))
EquationOutcome: Success
Substitutions:
	X = f(f(g(Z)))
	Y = f(g(Z))
\end{lstlisting}

\end{document}
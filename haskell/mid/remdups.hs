remdups :: [Int] -> [Int]
remdups = foldl apd []
        where apd [] a = [a]
              apd xs a = 
                  if (last xs) == a then xs
                  else xs ++ [a]
                  -- case last xs of
                  --      a -> xs
                  --      _ -> xs ++ [a]
remdups' :: [Int] -> [Int]
remdups' = foldr apd []
         where apd a [] = [a]
               apd a xs =
                   if (head xs) == a then xs
                   else a:xs

update :: [Int] -> [Int]
update xs = foldr cal  [] xs
       where cal a rs = (foldr (+) 0 xs) : rs

update' :: [Int] -> [Int]
update' xs = foldr (\a rs -> (foldr (+) 0 xs):rs)  [] xs


update'' :: [Int] -> [Int]
update'' xs = foldr (\_ rs -> s:rs)  [] xs
         where s = foldr (+) 0 xs

-- Is map a fold like function? It seems to be!
update2 :: [Int] -> [Int]
update2 xs = foldr f [] xs
        where f a [] = [a]
              f a rr@(r:rs) = (a + r) : (map (+a) rr)

update3 :: [Int] -> [Int]
update3 xs = foldr f xs xs
        where f a (r:rs) = 
        

data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday 
         deriving (Eq, Ord, Read, Bounded, Enum) 
                  
instance Show Day where
  show Monday = "this is monday"
  show Tuesday = "this is tuesday"
  

data Person = Person { firstName :: String
                       , lastName :: String
                       , age :: Int
                       } deriving (Show)


test :: Int -> Maybe (Int, Int, Int)
test 0 = Nothing
test x = Just (x-1, x, x+1)

data Vector a = Vector a a a deriving (Show)

vplus :: (Num a) => Vector a -> Vector a -> Vector a
vplus (Vector x y z) (Vector x' y' z') = Vector (x+x') (y+y') (z+z')


data Tree a = EmptyTree | Node a (Tree a) (Tree a)  deriving (Show)
-- data Tree Int = EmptyTree | Node Int  (Tree) (Tree)  -- deriving (Show)
-- instance Show (Tree Int) where
--   show EmptyTree = "Nil"
--   show (Node x left right) = (show x) ++ (show left) ++ (show right)

singleton :: a -> Tree a
singleton x = Node x EmptyTree EmptyTree

treeInsert :: (Ord a) => a -> Tree a -> Tree a 
treeInsert x EmptyTree = singleton x
treeInsert x (Node root left right) 
           | x == root = Node x left right
           | x < root = Node root (treeInsert x left) right
           | x > root = Node root left (treeInsert x right)
           

-- dfs, root first 
dfs :: (Show a) => Tree a -> IO ()
dfs EmptyTree = putStrLn "Nil"
dfs (Node x left right) = 
          do 
             putStrLn $ show x
             dfs left
             dfs right

-- bfs
bfs :: (Show a) => [Tree a] -> [a]
bfs [] = []
bfs (EmptyTree:xs) = bfs xs
bfs ((Node y left right):xs) = y : (bfs (xs ++ [left] ++ [right]))

bfs' :: (Show a) => Tree a -> [a]
bfs' x = bfs [x]

bfs'' :: (Show a) => Tree a -> IO ()
bfs'' x = sequence_ ( map (putStrLn.show) (bfs [x]))

freeTree :: Tree Char 
freeTree = 
         Node 'P' 
              (Node 'O' 
                    (Node 'L' (Node 'N' EmptyTree EmptyTree) 
                              (Node 'T' EmptyTree EmptyTree))
              (Node 'Y' 
                    (Node 'S' EmptyTree EmptyTree) 
                    (Node 'A' EmptyTree EmptyTree)) )
                                   (Node 'L' 
                                         (Node 'W' (Node 'C' EmptyTree EmptyTree) 
                                                   (Node 'R' EmptyTree EmptyTree))
                                   (Node 'A' (Node 'A' EmptyTree EmptyTree) 
                                   (Node 'C' EmptyTree EmptyTree)) )
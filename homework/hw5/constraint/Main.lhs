\documentclass[11pt]{article}
%% Literate Haskell script intended for lhs2TeX.

%include polycode.fmt
%format `union` = "\cup"
%format alpha = "\alpha"
%format gamma = "\gamma"
%format capGamma = "\Gamma"
%format tau = "\tau"
%format tau1 = "\tau_{1}"
%format tau2 = "\tau_{2}"
%format tau11 = "\tau_{11}"
%format tau12 = "\tau_{12}"
%format t12 = "t_{12}"
%format t1 = "t_{1}"
%format t1' = "t_{1}^{\prime}"
%format t2 = "t_{2}"
%format t2' = "t_{2}^{\prime}"
%format t3 = "t_{3}"
%format nv1 = "nv_{1}"

\usepackage{listings}
\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},language=haskell, basicstyle={\scriptsize}}

\usepackage{enumerate}
\usepackage{xcolor}

\usepackage{fullpage}
\usepackage{mathpazo}
\usepackage{graphicx}
\usepackage{color}
\usepackage[centertags]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{soul}

\usepackage{stmaryrd}

\title{Type reconstruction\footnote{CS558: Homework 5}}
\author{Zhenjie Chen}
\date{\today}

\begin{document}
\thispagestyle{empty}
\maketitle

%include ConstraintTyping.lhs

\section{Testing}

Here is the |Main| module which would accept filename as argument and then
reconstruct the term with type in that file based on type constraints derived
from the term itself. 

\begin{code}
module Main where

import qualified ConstraintTyping as C
import qualified AbstractSyntax as S
import qualified Unification as U
import qualified System.Environment
import qualified Data.Maybe as M

main :: IO()
main =
    do
      args <- System.Environment.getArgs
      let [sourceFile] = args
      source <- readFile sourceFile
      let tokens = S.scan source
      let term = S.parse tokens
      putStrLn ("---Original Term:---")
      putStrLn (show term)
      putStrLn ("\n---Reconstructed term:---")
      let term' = C.reconstructType term
      if term' == Nothing then
         error "Typing error..."      
      else 
          putStrLn (show $ M.fromJust term')
\end{code}

\section{Other test-cases}
\begin{code}
s1 = "pred ( if true then 0 else 0 fi)"
s2 = "if true then 0 else 0 fi"
s3 = "pred ( pred ( pred (0)))"
s4 = "if pred ( pred ( pred (0))) then 0 else iszero(0) fi"
s5 = "if pred (0) then 0 else true fi"
s6 = "pred( if true then 0 else 0 fi)"
s7 = "abs (x:Nat. iszero(x))"
s8 = "app (abs (x:Bool. x), false)"
s8n = "app (abs (x. x), false)"
s9 = "app (abs (x:Bool. if x then 0 else 0 fi), false)"
s9n = "app (abs (x. if x then 0 else 0 fi), false)"
splus = "app( app( fix( \
\     abs (plus: -> (Nat, ->(Nat, Nat)). \
\          abs(m:Nat. \
\                      abs(n:Nat. \
\                                 if iszero(m) then n\
\                                 else succ (app( app(plus, pred(m)), n))\
\                                 fi\
\                      )\
\            )\
\         )), succ(succ(0))),  succ(succ(succ(0))))"

splusn = "app( app( fix( \
\     abs (plus. \
\          abs(m. \
\                      abs(n. \
\                                 if iszero(m) then n\
\                                 else succ (app( app(plus, pred(m)), n))\
\                                 fi\
\                      )\
\            )\
\         )), succ(succ(0))),  succ(succ(succ(0))))"

s11 = "app ( app (abs ( f: -> (Nat, Bool) . f), abs ( x: Nat . iszero(x))) ,\
\          if iszero(0) then pred(0) else succ(0) fi )"
s11n = "app ( app (abs ( f . f), abs ( x . iszero(x))) ,\
\          if iszero(0) then pred(0) else succ(0) fi )"

s12n = "app(fix(abs(fact.abs(m.if iszero(m) then succ(0) else \
\ app(app(fix(abs(times.abs(m.abs(n.if iszero(m) then 0 else \
\ app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else \
\ succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,\
\ pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),succ(succ(0)))" 

s13n = "app( abs(x. if true then succ(0) else pred(0) fi), \
     \if true then pred(0) else 0 fi)"
s14n = "abs(x. if true then succ(0) else pred(0) fi)"

tm = S.parse . S.scan
tcs = C.deriveTypeConstraints . S.parse . S.scan 
unify = U.unify . C.encode . tcs 
decode = (\(outcome, unifyeqs) -> C.decode unifyeqs) . unify 
apply s =  C.applyTypeSubstitutionToTerm (decode s) (tm s)
\end{code}


\section{Compile and run}
Use |make|, {\it make all} or {\it make doc}, to manage all the compilation process as well as document generation.
To do the test, simply run: {./main ../test/PCF/FILE\_NAME}. 

Here is the testing output for |./runtest|,
\begin{lstlisting}
bash-3.2$ ./runtest 
Example 1: ../test/PCF/eq.npcf
---Original Term:---
app(app(fix(abs(eq.abs(x.abs(y.if iszero(x) then iszero(y) else if iszero(y) then 
    false else app(app(eq,pred(x)),pred(y)) fi fi)))),2),3)

---Reconstructed term:---
app(app(fix(abs(eq:->(Nat,->(Nat,Bool)).abs(x:Nat.abs(y:Nat.if iszero(x) then
    iszero(y) else if iszero(y) then false else app(app(eq,pred(x)),pred(y)) fi fi)))),2),3)

Example 2: ../test/PCF/exp.npcf
---Original Term:---
app(app(fix(abs(exp.abs(m.abs(n.if iszero(n) then 1 else app(app(fix(abs(times.abs(m.abs(n.if iszero(m) 
    then 0 else app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else 
        succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
            app(app(exp,m),pred(n))) fi)))),2),3)

---Reconstructed term:---
app(app(fix(abs(exp:->(X2,->(Nat,Nat)).abs(m:X2.abs(n:Nat.if iszero(n) then 1 else
     app(app(fix(abs(times:->(Nat,->(X10,Nat)).abs(m:Nat.abs(n:X10.if iszero(m) 
        then 0 else app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.
           if iszero(m) then n else succ(app(app(plus,pred(m)),n)) fi)))),n),
             app(app(times,pred(m)),n)) fi)))),m),app(app(exp,m),pred(n))) fi)))),2),3)

Example 3: ../test/PCF/facfac.npcf
---Original Term:---
app(fix(abs(fact.abs(m.if iszero(m) then 1 else app(app(fix(abs(times.abs(m.abs(n.if iszero(m) 
    then 0 else app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else
       succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
         app(fact,pred(m))) fi))),app(fix(abs(fact.abs(m.if iszero(m) then 1 else
           app(app(fix(abs(times.abs(m.abs(n.if iszero(m) then 0 else
             app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else 
              succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
                                            app(fact,pred(m))) fi))),3))

---Reconstructed term:---
app(fix(abs(fact:->(Nat,Nat).abs(m:Nat.if iszero(m) then 1 else app(app(fix(abs(times:->(Nat,->(X9,Nat)).
    abs(m:Nat.abs(n:X9.if iszero(m) then 0 else app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).
      abs(m:Nat.abs(n:Nat.if iszero(m) then n else succ(app(app(plus,pred(m)),n)) fi)))),n),
        app(app(times,pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),app(fix(abs(fact:->(Nat,Nat).
          abs(m:Nat.if iszero(m) then 1 else app(app(fix(abs(times:->(Nat,->(X64,Nat)).
            abs(m:Nat.abs(n:X64.if iszero(m) then 0 else app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).
              abs(m:Nat.abs(n:Nat.if iszero(m) then n else succ(app(app(plus,pred(m)),n)) fi)))),n),
                app(app(times,pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),3))

Example 4: ../test/PCF/fact.npcf
---Original Term:---
app(fix(abs(fact.abs(m.if iszero(m) then 1 else app(app(fix(abs(times.abs(m.abs(n.
    if iszero(m) then 0 else app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) 
      then n else succ(app(app(plus,pred(m)),n)) fi)))),n),
        app(app(times,pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),4)

---Reconstructed term:---
app(fix(abs(fact:->(Nat,Nat).abs(m:Nat.if iszero(m) then 1 else app(app(fix(abs(times:->(Nat,->(X9,Nat)).
    abs(m:Nat.abs(n:X9.if iszero(m) then 0 else app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).
      abs(m:Nat.abs(n:Nat.if iszero(m) then n else succ(app(app(plus,pred(m)),n)) fi)))),n),
        app(app(times,pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),4)

Example 5: ../test/PCF/iseven6.npcf
---Original Term:---
app(fix(abs(ie.abs(x.if iszero(x) then true else if iszero(pred(x)) then
     false else app(ie,pred(pred(x))) fi fi))),6)

---Reconstructed term:---
app(fix(abs(ie:->(Nat,Bool).abs(x:Nat.if iszero(x) then true else if iszero(pred(x)) then
     false else app(ie,pred(pred(x))) fi fi))),6)

Example 6: ../test/PCF/iseven7.npcf
---Original Term:---
app(fix(abs(ie.abs(x.if iszero(x) then true else if iszero(pred(x)) then
     false else app(ie,pred(pred(x))) fi fi))),7)

---Reconstructed term:---
app(fix(abs(ie:->(Nat,Bool).abs(x:Nat.if iszero(x) then true else if iszero(pred(x))
     then false else app(ie,pred(pred(x))) fi fi))),7)

Example 7: ../test/PCF/leq.npcf
---Original Term:---
app(app(fix(abs(leq.abs(x.abs(y.if iszero(x) then true else if iszero(y) then
   false else app(app(leq,pred(x)),pred(y)) fi fi)))),2),3)

---Reconstructed term:---
app(app(fix(abs(leq:->(Nat,->(Nat,Bool)).abs(x:Nat.abs(y:Nat.if iszero(x) then true else
   if iszero(y) then false else app(app(leq,pred(x)),pred(y)) fi fi)))),2),3)

Example 8: ../test/PCF/plus.npcf
---Original Term:---
app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else succ(app(app(plus,pred(m)),n)) fi)))),2),3)

---Reconstructed term:---
app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n else
    succ(app(app(plus,pred(m)),n)) fi)))),2),3)

Example 9: ../test/PCF/times.npcf
---Original Term:---
app(app(fix(abs(times.abs(m.abs(n.if iszero(m) then 0 else
    app(app(fix(abs(plus.abs(m.abs(n.if iszero(m) then n else 
      succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),2),3)

---Reconstructed term:---
app(app(fix(abs(times:->(Nat,->(X3,Nat)).abs(m:Nat.abs(n:X3.if iszero(m) then 0 
    else app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m)
        then n else succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),2),3)
\end{lstlisting}
\end{document}
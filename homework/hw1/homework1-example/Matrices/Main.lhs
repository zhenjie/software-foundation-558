CS558 Homework 1.1 
Firstname Lastname
\begin{code}
module Main where
  import Matrices

  test1 = rmvp [[2.0]] [4.0]

  main :: IO ()
  main =
    do
      print test1     
\end{code}

Compile as:
\begin{verbatim}
ghc --make -o matrices Main.lhs
\end{verbatim}

\documentclass[11pt]{article}
%% Literate Haskell script intended for lhs2TeX.

%include polycode.fmt
%format `union` = "\cup"
%format alpha = "\alpha"
%format gamma = "\gamma"
%format capGamma = "\Gamma"
%format tau = "\tau"
%format tau1 = "\tau_{1}"
%format tau2 = "\tau_{2}"
%format tau11 = "\tau_{11}"
%format tau12 = "\tau_{12}"
%format t12 = "t_{12}"
%format t1 = "t_{1}"
%format t1' = "t_{1}^{\prime}"
%format t2 = "t_{2}"
%format t2' = "t_{2}^{\prime}"
%format t3 = "t_{3}"
%format nv1 = "nv_{1}"

\usepackage{listings}
\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},language=haskell, basicstyle={\scriptsize}}

\usepackage{xcolor}

\usepackage{fullpage}
\usepackage{mathpazo}
\usepackage{graphicx}
\usepackage{color}
\usepackage[centertags]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{soul}



\usepackage{stmaryrd}

\title{Simply-typed call-by-value lambda-calculus with fix support\footnote{CS558: Homework 4(1)}}
\author{Zhenjie Chen}
\date{\today}
\begin{document}
\maketitle
\thispagestyle{empty}

% \newpage

\section{Introduction}
In this exercise, we will implement a type checker and an evaluator based on the 
small-step evaluation relation for the simply-typed call-by-value lambda-calculus 
with fix support. The concrete syntax of {\bf Type} and {\bf Term} is:
\begin{verbatim}
Type --> arr lpar Type comma Type rpar
       | bool
       | nat

Term --> identifier
       | abs lpar identifier colon Type fullstop Term rpar
       | app lpar Term comma Term rpar
       | true
       | false
       | if Term then Term else Term fi
       | 0
       | succ lpar Term rpar
       | pred lpar Term rpar
       | iszero lpar Term rpar
       | lpar Term rpar
       | fix lpar Term rpar
\end{verbatim}

\newpage

\section{Prologue}
\begin{code}
module Main where

import qualified System.Environment
import qualified Data.Map as M
import Data.List
import Data.Char(isAlpha, isAlphaNum)
import Text.Regex(subRegex, mkRegex)

class LatexShow tau where
  latexShow :: tau -> String
\end{code}

\section{Abstract-syntactic preliminaries}
\subsection{Types definition}
We need to add functional type, or more formally, type for abstraction.
\begin{code}
data Type  =  TypeArrow Type Type
           |  TypeBool
           |  TypeNat
           deriving Eq

instance Show Type where
  show  (TypeArrow tau1 tau2)   =  "->(" ++ show tau1 ++ "," ++ show tau2 ++ ")"
  show  TypeBool                =  "Bool"
  show  TypeNat                 =  "Nat"

instance LatexShow Type where
  latexShow  (TypeArrow tau1 tau2)    =  "$\\rightarrow$ (" ++ latexShow tau1
                                         ++ ", " ++ latexShow tau2 ++ ")"
  latexShow  TypeBool                 =  "Bool"
  latexShow  TypeNat                  =  "Nat"
\end{code}

\subsection{Terms definition}
Add terms' definition for {\it fix}
\begin{code}
type Var  =  String

data Term  = Fix Term 
           |  Var Var
           |  Abs Var Type Term
           |  App Term Term
           |  Tru
           |  Fls
           |  If Term Term Term
           |  Zero
           |  Succ Term
           |  Pred Term
           |  IsZero Term
           deriving Eq

type Value = Term

data Contex = Hole
            | IfContex Contex Term Term
            | SuccContex Contex 
            | PredContex Contex
            | IsZeroContex Contex      
            | Appl Contex Term
            | Appr Value Contex
            | FixContex Contex
            deriving (Eq, Show)
 

type Redex = Term

instance Show Term where
  show (Var x)        =  x
  show (Abs x tau t)  =  "abs(" ++ x ++ ":" ++ show tau ++ "." ++ show t ++ ")"
  show (App t1 t2)    =  "app(" ++ show t1  ++ "," ++ show t2 ++ ")"
  show Tru            =  "true"
  show Fls            =  "false"
  show (If t1 t2 t3)  =  "if " ++ show t1 ++ " then " ++ show t2 ++ " else " ++ show t3 ++ " fi"
  show Zero           =  "0"
  show (Succ t)       
       | isNumericValue t = showAsNum t 1
       | otherwise = "succ(" ++ show t ++ ")"
           where showAsNum Zero num = show num
                 showAsNum (Succ t) num = showAsNum t (num+1)
  show (Pred t)       =  "pred(" ++ show t ++ ")"
  show (IsZero t)     =  "iszero(" ++ show t ++ ")"
  show (Fix t)        = "fix(" ++ show t ++ ")"

instance LatexShow Term where
  latexShow  (Var x)            =  x
  latexShow  (Abs x tau t)      =  "$\\lambda$" ++ x ++ ": " ++ latexShow tau
                                   ++ ". " ++ latexShow t
  latexShow  (App t1 t2)        =  "$\\blacktriangleright$ (" ++ latexShow t1  ++ ", " 
                                   ++ latexShow t2 ++ ")"
  latexShow  Tru                =  "true"
  latexShow  Fls                =  "false"
  latexShow  (If t1 t2 t3)      =  "if " ++ latexShow t1 ++ " then " ++ latexShow t2
                                   ++ " else " ++ latexShow t3 ++ " fi"
  latexShow Zero                =  "0"
  latexShow (Succ t)            =  "succ(" ++ latexShow t ++ ")"
  latexShow (Pred t)            =  "pred(" ++ latexShow t ++ ")"
  latexShow (IsZero t)          =  "iszero(" ++ latexShow t ++ ")"
  latexShow (Fix t)             = "fix(" ++ latexShow t ++ ")"
\end{code}

\newpage
\subsection{Binding and free variables}
We define function {\it fv} to collect all the free variables of a term.
\begin{code}
fv :: Term -> [Var]
fv (Var x)        =  [x]
fv (Abs x _ t1)   =  filter (/=x) (fv t1)
fv (App t1 t2) = fv t1 ++ fv t2
fv (If t1 t2 t3) = fv t1 ++ fv t2 ++ fv t3
fv (Succ t1) = fv t1
fv (Pred t1) = fv t1
fv (IsZero t1) = fv t1
fv (Fix t1)    = fv t1
fv _ = []
\end{code}

\subsection{Substitution}
Substitution: |subst x s t|, or $[x \mapsto s]t$ in Pierce,
is the result of substituting |s| for all the free occurrence of |x| in |t|.
This implementation of |subst| has been simplified for {\it Simply-typed 
call-by-value lambda-calculus with fix support}.

\begin{code}
subst :: Var -> Term -> Term -> Term
subst x v t =
      let fvars = fv t in 
         if not $ x `elem` fvars 
            then t
            else 
              case t of 
                    Var x -> v
                    Abs x' typeT1 t1 -> Abs x' typeT1 (subst x v t1)
                    App t1 t2 -> App (subst x v t1) (subst x v t2)
                    If t1 t2 t3 -> If (subst x v t1) (subst x v t2) (subst x v t3)
                    Succ t1 -> Succ (subst x v t1)
                    Pred t1 -> Pred (subst x v t1)
                    IsZero t1 -> IsZero (subst x v t1)
                    Fix t1 -> Fix (subst x v t1)
                    _ -> t
\end{code}

\newpage
\section{Small-step evaluation relation}
Here are some helper functions for Small-step evaluation:
\begin{code}
isValue :: Term -> Bool
isValue  (Abs _ _ _)  =  True
isValue  Tru          =  True
isValue  Fls          =  True
isValue  Zero         =  True
isValue  (Succ t)     =  isNumericValue t
isValue  _            =  False

isNumericValue :: Term -> Bool
isNumericValue  Zero      =  True
isNumericValue  (Succ t)  =  isNumericValue t
isNumericValue  _         =  False
\end{code}

Function |eval'| is based on one-step evaluation, ie. it takes a term and apply Small-step evaluation 
to this term, then return the evaluated term. It contains three phases: decompose term into 
context and possible redex(by |makeContex|), reduce(by |eval_redex|), recompose(by |eval_helper|). 
We extend the evaluator in 
homework 3 by adding patterns for fix. Here comes the evaluator for 
{\bf Simply-typed call-by-value lambda-calculus with fix support}.
\begin{code}
eval_redex :: Redex -> Term
eval_redex (If Tru t2 _) = t2
eval_redex (If Fls _ t3) = t3
eval_redex (Pred Zero) = Zero
eval_redex (Pred (Succ nv)) 
           | isNumericValue nv = nv
eval_redex (IsZero Zero) = Tru
eval_redex (IsZero (Succ nv)) 
           | isNumericValue nv = Fls
eval_redex (App v1 v2) =
           -- v1 should be an abstraction
           case v1 of 
                (Abs x tpeT11 t12) -> (subst x v2 t12)
                _ -> error "Expected abstracton"
eval_redex t@(Fix v1) =
           -- v1 should be an abstracton           
           case v1 of
                (Abs x typeT1 t2) -> subst x t t2
                _ -> error "Expected abstracton"

eval_helper :: (Contex, Redex) -> Term  
eval_helper (Hole, redex) = eval_redex redex
eval_helper (IfContex ctx t2 t3, redex) = If (eval_helper (ctx, redex)) t2 t3
eval_helper (SuccContex ctx, redex) = Succ (eval_helper (ctx, redex))
eval_helper (PredContex ctx, redex) = Pred (eval_helper (ctx, redex))
eval_helper (IsZeroContex ctx, redex) = IsZero (eval_helper (ctx, redex))
eval_helper (Appl ctx t2, redex) = App (eval_helper (ctx, redex)) t2
eval_helper (Appr t1 ctx, redex) = App t1 (eval_helper (ctx, redex))
eval_helper (FixContex ctx, redex) = Fix (eval_helper (ctx, redex))


makeContex :: Term -> Maybe (Contex, Redex)
makeContex t@(If Tru t2 _) = Just(Hole, t)
makeContex t@(If Fls _ t3) = Just(Hole, t)
makeContex (If t1 t2 t3) = -- t1 should contain at lease one redex
           case makeContex t1 of
                Just (ctx, redex) -> Just (IfContex ctx t2 t3, redex)
                Nothing -> Nothing
makeContex (Succ t) =  
           case makeContex t of
                Just (ctx, redex) -> Just (SuccContex ctx, redex)
                Nothing -> Nothing
makeContex t@(Pred Zero) = Just (Hole, t)
makeContex t@(Pred (Succ nv)) 
           |  isNumericValue nv = Just (Hole, t)
makeContex (Pred t) =
           case makeContex t of
                Just (ctx, redex) -> Just (PredContex ctx, redex)
                Nothing -> Nothing
makeContex t@(IsZero Zero) = Just (Hole, t)
makeContex t@(IsZero (Succ nv))
           | isNumericValue nv = Just (Hole, t)
makeContex (IsZero t) = 
           case makeContex t of
                Just (ctx, redex) -> Just (IsZeroContex ctx, redex)
                Nothing -> Nothing
makeContex t@(App t1 t2) 
           | isValue t1 =
                     if isValue t2 then Just (Hole, t) 
                     else 
                             case makeContex t2 of
                                  Just (ctx, redex) -> Just (Appr t1 ctx, redex)
                                  Nothing -> Nothing
           | otherwise =  
                     case makeContex t1 of 
                          Just (ctx, redex) -> Just (Appl ctx t2, redex)
                          Nothing -> Nothing
makeContex t@(Fix (Abs x typeT1 t2)) = Just (Hole, t)
makeContex (Fix t1) =
           case makeContex t1 of
                Just (ctx, redex) -> Just (FixContex ctx, redex)
                Nothing -> Nothing
makeContex _ = Nothing
\end{code}

Here is the control function |eval'| which recursively calls eval' until reaching
the normal form if the term is well-typed.
\begin{code}
eval' :: Term -> Term 
eval' t = 
      case makeContex t of
           Just (Hole, redex) -> eval' $ eval_redex redex
           Just (ctx, redex) -> eval' $ eval_helper (ctx, redex)
           Nothing -> t  -- stuck or value?
\end{code}

\newpage
\section{Typing}
\subsection{Typing context~($\Gamma$) definition}
We recursively define the typing context~($\Gamma$) to make it easier to look up a type in
$\Gamma$.
\begin{code}
data TypingContext  =  Empty
                    |  Bind TypingContext Var Type
                    deriving Eq
instance Show TypingContext where
  show Empty                  =  "<>"
  show (Bind capGamma x tau)  =  show capGamma ++ "," ++ x ++ ":" ++ show tau

contextLookup :: Var -> TypingContext -> Maybe Type
contextLookup x Empty  =  Nothing
contextLookup x (Bind capGamma y tau)
  | x == y      =  Just tau
  | otherwise   =  contextLookup x capGamma
\end{code}

\subsection{Type checking}
Following the typing rules, type the sub terms step by step and collect the basic types 
to build up the final type of the whole term.

\begin{code}
typing :: TypingContext -> Term -> Maybe Type
typing _ Tru = Just TypeBool
typing _ Fls = Just TypeBool
typing _ Zero = Just TypeNat
typing capGamma (Pred t) = 
       case typing capGamma t of
            Just TypeNat  -> Just TypeNat
            Nothing -> Nothing
typing capGamma (Succ t) = 
       case typing capGamma t of
            Just TypeNat -> Just TypeNat
            Nothing -> Nothing
typing capGamma (IsZero t) =
       case typing capGamma t of
            Just TypeNat -> Just TypeBool
            Nothing -> Nothing
typing capGamma term@(If tc t1 t2) = 
       case typing capGamma tc of 
            Nothing -> Nothing
            Just TypeBool -> 
                 case typing capGamma t1 of
                      Nothing -> Nothing
                      Just typeT1 -> 
                           case typing capGamma t2 of
                                Nothing -> Nothing
                                Just typeT2 -> 
                                     if typeT1 == typeT2 
                                        then return typeT1
                                        else error $ "Expected type: " ++ show typeT1 ++ " for \n'" 
                                             ++ show t2 ++ "'\nin \n" ++ show term ++ "\nwith actual type: " ++ show typeT2
            _ -> error "Expected Boolean type for 'if'"
typing capGamma (Abs x typeT1 t2) =
       case typing (Bind capGamma x typeT1) t2 of
            Nothing -> Nothing
            Just typeT2 -> Just (TypeArrow typeT1 typeT2)       
typing capGamma term@(App t1 t2) = 
       case typing capGamma t1 of
            Nothing -> Nothing
            Just (TypeArrow typeT1 typeT2) -> 
                 case typing capGamma t2 of
                      Nothing -> Nothing
                      Just typeT3 ->
                           if typeT1 == typeT3
                              then return typeT2
                              else error $ "Expected type: " ++ show typeT1 ++ " for \n'" ++ show t2 
                                   ++ "'\nin \n" ++ show term ++ "\nwith actual type: " ++ show typeT3
typing capGamma (Var x) = contextLookup x capGamma
typing capGamma (Fix t) = 
       case typing capGamma t of
            Nothing -> Nothing
            Just (TypeArrow typeT1 typeT2) ->
                 if typeT1 == typeT2 then return typeT1
                 else error "Typing error: Fix"
\end{code}

Again, here is the control code for type checking:
\begin{code}
typeCheck :: Term -> Type
typeCheck t =
  case typing Empty t of
    Just tau -> tau
    _ -> error "type error"
\end{code}

\newpage
\section{Scanning and parsing}

\subsection{Token definition}
Most of the |Token| definition is the same as in homework 2. we will use |Identifier String|
to denote the variables in lambda calculus.
\begin{code}
data Token  =  Identifier String
            |  AbsKeyword
            |  AppKeyword
            |  TrueKeyword
            |  FalseKeyword
            |  IfKeyword
            |  ThenKeyword
            |  ElseKeyword
            |  FiKeyword
            |  BoolKeyword
            |  ZeroKeyword
            |  SuccKeyword
            |  PredKeyword
            |  IsZeroKeyword
            |  NatKeyword
            |  LPar
            |  RPar
            |  Comma
            |  Colon
            |  FullStop
            |  RightArrow
            |  FixKeyword
            deriving (Eq, Show)
\end{code}

\subsection{Scanner}
We still use |lex| function to scan the source code and generate a list of tokens. The 
only new and funny stuff is to judge the input as an legal identifier. Function |verifyId|
will serve for that.
\begin{code}
scan :: String -> [Token]
scan s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] 
      | token `elem` keywordsList -> hash  M.! token : scan tl
      | verifyId token -> Identifier token : scan tl
      | otherwise -> error  $ "Invalid token: " ++ token 
      where 
        keywordsList = 
          ["true", "false", "if", "then", "else", "fi", "0", "succ", "(", 
          ")", "pred", "iszero", "abs", "app", ":", ".", "Nat", "Bool",
           ",", "->", "fix"]
        tokenList = 
          [TrueKeyword, FalseKeyword, IfKeyword, ThenKeyword, ElseKeyword, 
           FiKeyword, ZeroKeyword, SuccKeyword, LPar, RPar, PredKeyword, 
           IsZeroKeyword, AbsKeyword, AppKeyword, Colon, FullStop, 
           NatKeyword, BoolKeyword, Comma, RightArrow, FixKeyword]
        hash = M.fromList $ zip keywordsList tokenList

verifyId :: [Char] -> Bool
verifyId [] = False
verifyId id@(x:xs) = and $ isAlpha x : map (isAlphaNum) id
\end{code}


\subsection{Parser}
Since we introduce types in the new term, we need to parse the type when there's any
abstraction in source code. |parseType| will do it for us. The others are almost the 
same as what we have done in homework2.
\begin{code}
parseType :: [Token] -> Maybe (Type, [Token])
parseType (RightArrow:LPar:tl) =
  case parseType tl of
    Just (tau1, Comma:tl') ->
      case parseType tl' of
        Just (tau2, RPar:tl'') -> Just (TypeArrow tau1 tau2, tl'')
        _ -> Nothing
    _ -> Nothing
parseType (BoolKeyword:tl) = Just (TypeBool, tl)
parseType (NatKeyword:tl) = Just (TypeNat, tl)
parseType _ = error "Syntax error in type declaration"

parseTerm :: [Token] -> Maybe (Term, [Token])
parseTerm (TrueKeyword:tl) = Just (Tru, tl)
parseTerm (FalseKeyword:tl) = Just (Fls, tl)
parseTerm (ZeroKeyword:tl) = Just(Zero, tl)
parseTerm (PredKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Pred t, tl1)
    _ -> error "Syntax error after 'pred'"
parseTerm (SuccKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Succ t, tl1)
    _ -> error "Syntax error after 'succ'"
parseTerm (IfKeyword:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t1, ThenKeyword:tl1) -> 
      case parseTerm tl1 of
        Nothing -> Nothing
        Just(t2, ElseKeyword:tl2) ->
          case parseTerm tl2 of
            Nothing -> Nothing
            Just(t3, FiKeyword:tl3) -> Just(If t1 t2 t3, tl3)
            _ -> error "Syntax error after 'else'"
    _ -> error "Syntax error after 'if'"
parseTerm (IsZeroKeyword:LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(IsZero t, tl1)
    _ -> error "Syntax error after 'iszero'"
parseTerm (LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just (t, tl1)
    _ -> error "Mismatching '(' and ')'"
parseTerm (Identifier s:tl) = Just (Var s, tl)
parseTerm (AbsKeyword:LPar:tl) =
  case parseTerm tl of
       Nothing -> Nothing
       Just (Var s, Colon:tl1) -> 
            case parseType tl1 of
                 Nothing -> Nothing
                 Just (TypeArrow tau1 tau2, FullStop:tl3) ->
                      case parseTerm tl3 of 
                           Nothing -> Nothing
                           Just(t1, RPar:tl3') -> Just(Abs s (TypeArrow tau1 tau2) t1, tl3')
                 Just(valueType, FullStop:tl2) -> 
                      case parseTerm tl2 of
                           Nothing -> Nothing
                           Just(t1, RPar:tl2') -> Just(Abs s valueType t1, tl2')
parseTerm (AppKeyword:LPar:tl) = 
  case parseTerm tl of
       Nothing -> Nothing
       Just(t1, Comma:tl1) ->
           case parseTerm tl1 of
                Nothing -> Nothing
                Just(t2, RPar:tl2) -> Just(App t1 t2, tl2)
parseTerm (FixKeyword:LPar:tl) =
  case parseTerm tl of 
       Nothing -> Nothing                    
       Just(t1, RPar:tl1) -> Just(Fix t1, tl1)
parseTerm _ = Nothing


parse :: [Token] -> Term
parse ts =
  case parseTerm ts of
    Just (t, []) -> t
    Just (t, _) -> error "syntax error: spurious input at end"
    Nothing -> error "syntax error"
\end{code}

\newpage
\section{Epilogue}
\subsection{Main control flow}
\begin{code}
main :: IO ()
main =
    do
      args <- System.Environment.getArgs
      let [sourceFile] = args
      source <- readFile sourceFile
      let tokens = scan source
      let term = parse tokens
      putStrLn ("---Term:---")
      putStrLn (show term)
      putStrLn ("---Type:---")
      putStrLn (show (typeCheck term))
      putStrLn ("---Normal form:---")
      putStrLn (show (eval' term))
\end{code}

\subsection{Compile}
Refert to Makefile for more information,
\begin{enumerate}
\item {\it make pcf} will build the executable file {\bf pcf};
\item {\it make doc} will build the document {\bf PCF.pdf}
\item {\it make all} will build the executable file {\bf pcf}, document {\bf PCF.pdf} and open it with {\bf READER};
\item {\it make clean} clean  the project.
\end{enumerate}

\subsection{Testing}
After compiling the code with |make|, it is convenient to run the test using the 
simple testing script |runtest|. {\it ./runtest} would run all the test-cases.

\subsection{Test log}
Here is the log for running {\it ./runtest}, including all test-cases.
Note that the output of |succ| has been interpreted for a better way to read, thus
|succ(...(succ(0)))| would be printed out as the real value.

\begin{lstlisting}
bash-3.2$ ./runtest 
Example 1: eq.pcf
---Term:---
app(app(fix(abs(eq:->(Nat,->(Nat,Bool)).abs(x:Nat.abs(y:Nat.
   if iszero(x) then iszero(y) else if iszero(y) then false else
         app(app(eq,pred(x)),pred(y)) fi fi)))),2),3)
---Type:---
Bool
---Normal form:---
false

Example 2: exp.pcf
---Term:---
app(app(fix(abs(exp:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(n) then 1 else 
    app(app(fix(abs(times:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then 0 else 
      app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n 
        else succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
          app(app(exp,m),pred(n))) fi)))),2),3)
---Type:---
Nat
---Normal form:---
8

Example 3: facfac.pcf
---Term:---
app(fix(abs(fact:->(Nat,Nat).abs(m:Nat.if iszero(m) then 1 else 
  app(app(fix(abs(times:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then 0 else 
    app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n else 
      succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),app(fact,pred(m))) fi))),
        app(fix(abs(fact:->(Nat,Nat).abs(m:Nat.if iszero(m) then 1 else 
           app(app(fix(abs(times:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then 0 else 
              app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n else 
                 succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
                  app(fact,pred(m))) fi))),3))
---Type:---
Nat
---Normal form:---
720

Example 4: fact.pcf
---Term:---
app(fix(abs(fact:->(Nat,Nat).abs(m:Nat.if iszero(m) then 1 else
  app(app(fix(abs(times:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then 0 else
    app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n else
      succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),m),
        app(fact,pred(m))) fi))),4)
---Type:---
Nat
---Normal form:---
24

Example 5: iseven6.pcf
---Term:---
app(fix(abs(ie:->(Nat,Bool).abs(x:Nat.if iszero(x) then true else if iszero(pred(x)) then false 
      else app(ie,pred(pred(x))) fi fi))),6)
---Type:---
Bool
---Normal form:---
true

Example 6: iseven7.pcf
---Term:---
app(fix(abs(ie:->(Nat,Bool).abs(x:Nat.if iszero(x) then true else if iszero(pred(x)) then false 
    else app(ie,pred(pred(x))) fi fi))),7)
---Type:---
Bool
---Normal form:---
false

Example 7: leq.pcf
---Term:---
app(app(fix(abs(leq:->(Nat,->(Nat,Bool)).abs(x:Nat.abs(y:Nat.if iszero(x) then true 
    else if iszero(y) then false else
        app(app(leq,pred(x)),pred(y)) fi fi)))),2),3)
---Type:---
Bool
---Normal form:---
true

Example 8: plus.pcf
---Term:---
app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n 
   else succ(app(app(plus,pred(m)),n)) fi)))),2),3)
---Type:---
Nat
---Normal form:---
5

Example 9: times.pcf
---Term:---
app(app(fix(abs(times:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then 0 else
     app(app(fix(abs(plus:->(Nat,->(Nat,Nat)).abs(m:Nat.abs(n:Nat.if iszero(m) then n else
        succ(app(app(plus,pred(m)),n)) fi)))),n),app(app(times,pred(m)),n)) fi)))),2),3)
---Type:---
Nat
---Normal form:---
6
\end{lstlisting}

\end{document}

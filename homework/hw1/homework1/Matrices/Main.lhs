\documentclass[11pt,a4paper]{article}
\usepackage{url}

% define \ignore{}
\long\def\ignore#1{}


%include lhs2TeX.fmt
%include lhs2TeX.sty

% redefine {code}
\usepackage{listings}
\usepackage{xcolor}
\lstloadlanguages{Haskell}
\lstnewenvironment{code}{
    {\lstset{}%
      \csname lst@SetFirstLabel\endcsname}
    {\csname lst@SaveFirstLabel\endcsname}
    \lstset{
      basicstyle=\small\ttfamily,
      frame=single,
      flexiblecolumns=false,
      basewidth={0.5em,0.45em},
      literate={+}{{$+$}}1 {/}{{$/$}}1 {*}{{$*$}}1 {=}{{$=$}}1
               {>}{{$>$}}1 {<}{{$<$}}1 {\\}{{$\lambda$}}1
               {\\\\}{{\char`\\\char`\\}}1
               {->}{{$\rightarrow$}}2 {>=}{{$\geq$}}2 {<-}{{$\leftarrow$}}2
               {<=}{{$\leq$}}2 {=>}{{$\Rightarrow$}}2 
               {\ .}{{$\circ$}}2 {\ .\ }{{$\circ$}}2
               {>>}{{>>}}2 {>>=}{{>>=}}2
    }}{}

% no indention
% \setlength{\parindent}{0in}

\begin{document}
\title{CS558: Homework 1}
\author{Zhenjie Chen}
\date{\today}

\maketitle

%include Matrices.lhs

%include Testcase.lhs

\section{Miscellaneous}
\subsection{Compile}
\begin{verbatim}
ghc --make -o matrices Main.lhs
\end{verbatim}
        
\subsection{Run test}
Fire the command {\it ./matrices} to run the test or run the following commands in {\bf ghci}.
\ignore{
\begin{code}
module Main where
import Testcase
import Matrices
import Data.List
import Test.QuickCheck
import Test.QuickCheck.All

main = do
  putStrLn "Testing rmmp and rmvp:"
  quickCheck prop_rmmp
  putStrLn "Testing rmt' with transpose:"
  quickCheck prop_rmt'_transpose 
  putStrLn "Testing rmt with transpose:"
  quickCheck prop_rmt_transpose
  putStrLn "Testing rmt and rmt':"
  quickCheck prop_rmt'_rmt
\end{code}
}

\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},frame=single, language=haskell, numbers=left, numberstyle=\tiny\color{gray}}
\begin{lstlisting}
  :l Testcase
  import Test.QuickCheck
  quickCheck prop_rmmp
  quickCheck prop_rmt'_transpose 
  quickCheck prop_rmt_transpose
  quickCheck prop_rmt'_rmt
\end{lstlisting}

\subsection{Test log}
As listed in file: {\it Matrices.testlog.txt}.
\lstset{basicstyle=\footnotesize, frame=single, language=haskell}
\begin{lstlisting}
%include Matrices.testlog.txt
\end{lstlisting}

\end{document}
import Data.Char

hello [] = []
hello ('t':'h':'e':s) 
  | s == [] || isSpace (s!!0) = "the"
  | otherwise = "thex"
hello ('h':'h':'l':s) 
  | s == [] || isSpace (s!!0) = "hhl"
  | otherwise = "hhlx"
hello "abc" = "bac"
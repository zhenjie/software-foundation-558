nondec :: Ord a => [a] -> Bool
nondec xs = f (zip xs (tail xs))
		where f = and.(map (\(a,b) -> a <= b))


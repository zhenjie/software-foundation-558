import System.Environment
import Data.Char
import Text.Regex
import Data.List
import qualified Data.Map as M

main :: IO()
main = 
  do args <- getArgs
     let [fileSource] = args
     -- putStrLn fileSource
     content <- readFile fileSource
     -- putStrLn content
     -- print content
     let tokens = scan3 content
     -- let tokens = scan3 " if then "
     print tokens

data Token = TrueKeyword
           | FalseKeyword
           | IfKeyword
           | ThenKeyword
           | ElseKeyword
           | FiKeyword
           | ZeroKeyword
           | SuccKeyword
           | PredKeyword
           | IsZeroKeyword
           | LPar
           | RPar
           deriving (Eq, Show)
     
-- We will not detect the error of "if)(then"
con s = s == [] || isSpace (s!!0) || (s!!0) == '(' || (s!!0) == ')'

scan :: String -> [String]
scan [] = []
scan ('i':'f':s)  
  | con s = "IF" : scan s
  | otherwise = error "Invalid token if"
scan ('t':'r':'u':'e':s)
  | con s= "TRUE" : scan s
  | otherwise = error "Invalid token true"
scan ('f':'a':'l':'s':'e':s)
  | con s= "FALSE" : scan s
  | otherwise = error "Invalid token false"
scan ('t':'h':'e':'n':s)
  | con s= "THEN" : scan s
  | otherwise = error "Invalid token then"
scan ('e':'l':'s':'e':s)
  | con s= "ELSE" : scan s
  | otherwise = error "Invalid token else"
scan ('f':'i':s)
  | con s= "FI" : scan s
  | otherwise = error "Invalid token fi"
scan ('s':'u':'c':'c':s)
  | con s= "SUCC" : scan s
  | otherwise = error "Invalid token succ"
scan ('p':'r':'e':'d':s)
  | con s= "pred" : scan s
  | otherwise = error "Invalid token pred"
scan ('i':'s':'z':'e':'r':'o':s)
  | con s= "iszero" : scan s
  | otherwise = error "Invalid token iszero"
scan ('0':s) 
  | con s= "0" : scan s
  | otherwise = error "Invalid token 0"
scan ('(':s) = "lpar" : scan s
scan (')':s) = "rpar" : scan s
scan (t:s) 
  | isSpace(t) = scan s
  | otherwise = error "Invalid token no space"


-- scan1 :: String -> [String]
-- scan1 [] = []
-- scan1 s --  = [sub | sub <- ["if", "else", "then"], sub `isPrefixOf` s ] ++ scan1 sub

-- without incorrect case
scan3 :: String -> [Token]
scan3 s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] -> hash M.! token : scan3 tl
      where hash = M.fromList[ ("true", TrueKeyword), ("false", FalseKeyword), ("if", IfKeyword), ("then", ThenKeyword), ("else", ElseKeyword), ("fi", FiKeyword), ("0", ZeroKeyword), ("succ", SuccKeyword), ("(", LPar), (")", RPar), ("pred", PredKeyword), ("iszero", IsZeroKeyword)]

-- correct !!
scan4 :: String -> [Token]
scan4 s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] 
      | token `elem` keywordsList -> hash M.! token : scan4 tl
      | otherwise -> error $ "\nInvalid token: '" ++ token ++ "'"
      where 
        keywordsList = ["true", "false", "if", "then", "else", "fi", "0", "succ", "(", ")", "pred", "iszero"]
        tokenList = [TrueKeyword, FalseKeyword, IfKeyword, ThenKeyword, ElseKeyword, FiKeyword, ZeroKeyword, SuccKeyword, LPar, RPar, PredKeyword, IsZeroKeyword]
        hash = M.fromList $ zip keywordsList tokenList


scan2 :: String -> [String]
scan2 [] = []
scan2 s = tokenize ( splitRegex (mkRegex "\\s") s)

tokenize :: [String] -> [String]
tokenize (x:xs) | "" == x = tokenize xs
                | "if" == x = "IF": tokenize xs
                | "then" == x = "THEN": tokenize xs
                | "else" == x = "ELSE": tokenize xs
                | "true" == x = "TRUE": tokenize xs
                | "false" == x = "FALSE": tokenize xs
                | "fi" == x = "FI": tokenize xs
                | otherwise = error "Invalid program"
                              
                              
                              
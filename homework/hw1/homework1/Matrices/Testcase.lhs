\section{Testing}

\subsection{Introduction to quick-check}
QuickCheck is a library for random testing of program properties.


The programmer provides a specification of the program, in the form of properties which functions should satisfy, and QuickCheck then tests that the properties hold in a large number of randomly generated cases.


Specifications are expressed in Haskell, using combinators defined in the QuickCheck library. QuickCheck provides combinators to define properties, observe the distribution of test data, and define test data generators.\footnote{\url{http://hackage.haskell.org/package/QuickCheck-2.5}}


\subsection{Import modules and declare types}
Since the types are not exported from Matrices, we give the definition again in our
$Testcase$ module.

\begin{code}
-- Testing model
module Testcase (prop_rmmp, prop_equal, prop_rmt_transpose, 
                 prop_rmt'_transpose, prop_rmt'_rmt, prop_twice_transpose) where

-- import
import Matrices
import Data.List
import Test.QuickCheck
import Test.QuickCheck.All

-- types
type Realvector = [Double]
type Realmatrix = [[Double]]
\end{code}

\subsection{Testing matrix multiplication}
Given three compatible matrix, $A(m\times n), B(n\times p), C(n\times p)$, the following equation should be true.
\begin{equation}
A\times (B\times C) = (A\times B) \times C.
\end{equation}
The property we are going to test is based on this rule. See the following definition\label{prop:1},
\begin{code}
prop_rmmp xs ys zs = rmmp (rmmp xs ys) zs == rmmp xs (rmmp ys zs)
  where types = (xs :: Realmatrix, ys :: Realmatrix, zs :: Realmatrix)
\end{code}


I would like to point out that, the property described above will not hold for all cases because the type of elements
in the matrix is {\bf Double} which means $(x*y)*z == x*(y*z)$ is false in some case. See the test bellow,
\begin{code}
prop_equal x y z = (x*y) * z == x * (y*z)
  where types = (x::Double, y::Double, z::Double)
\end{code}

Fire the test by typing, 
\begin{code}% Not real code
:l Testcase
quickCheck prop_equal
\end{code}%

And it will also fail when the matrices is not complete, in some case. You can see those cases in the test log. So please 
run the test more than once.


As you can see, I do not define the property for testing function $rmvp$ since we call $rmvp$ in my definition of $rmmp$ thus it is tested when $rmmp$ is fully tested.

\subsection{Testing matrix transpose}
To test $rmt$ and $rmt'$ function, we defined three properties:
\begin{description}

\item [Property 1] The result of $rmt'$ equals to the result of $Data.List.transpose$. Here is the code,
\begin{code}
prop_rmt'_transpose xs = rmt' xs == transpose xs
\end{code}

\item [Property 2] The result of $rmt$ equals to the result of $Data.List.transpose$. See the code,
\begin{code}
prop_rmt_transpose xs = rmt xs == transpose xs
\end{code}

\item [Property 3] $rmt'$ and $rmt$ return the same result. Code as following,
\begin{code}
prop_rmt'_rmt xs = rmt' xs == rmt xs
\end{code}

\end{description}

\ignore{
\begin{code}
-- helper functions
same'length [] = True
same'length xs = and $map (== (head p)) p  
  where p = map length xs
\end{code}
}


When applying  {\it Data.List.transpose} twice to a matrix, it will not return the origin
matrix if the matrix is not complete, for example: m = [[1, 2, 3], [2, 3], [5], [6,7]]. 
We can test it using the following property description. 
\begin{code}
prop_twice_transpose xs = transpose (transpose xs) == xs
  where types = xs :: Realmatrix
\end{code}
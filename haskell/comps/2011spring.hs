-- 2011 January ProgLang
import Data.List 

-- 1.1 Combinatorics
{-
   issue: combinations 2 "test" would return
          ["te","ts","tt","es","et","st"] which might be incorrect
-}
combinations :: Int -> String -> [String]
combinations _ [] = []
combinations 1 s = [[x] | x <- s] 
combinations n (s:sl) = map (s:) (combinations (n-1) sl) ++ (combinations n sl)

-- might be better
combinations' :: Int -> String -> [String]
combinations' _ [] = []
combinations' 1 s = [[x] | x <- s] 
combinations' n (s:sl) = map (s:) (combinations (n-1) sl') ++ (combinations n sl')
              where sl' = removeItem s sl

-- helper
removeItem :: Eq a => a -> [a] -> [a]
removeItem _ [] = []
removeItem x (y:ys)
		| x ==y = removeItem x ys
		| otherwise = y: removeItem x ys

-- 1.2 String editing
insertions :: String -> [String]
insertions [] = [[x] | x <- "AGCT"]
insertions str@(s:sl) = [ a:str | a <- "AGCT"]  ++ (map (s:) (insertions sl))


-- note that, map ('A':) [] will return []
deletions :: String -> [String]
deletions [] = []
deletions (s:[]) = []
deletions (s1:s2:[]) = [[s1], [s2]]
deletions (s:sl) = sl :  map (s:) (deletions sl)

substitutions :: String -> [String]
substitutions [] = []
substitutions (s:sl) = [a:sl | a <- "AGCT"] ++ map (s:) (substitutions sl)

transpositions :: String -> [String]
transpositions [] = []
transpositions (s:[]) = [[s]]
transpositions (s1:s2:[]) = [s2:[s1]]
transpositions (s1:s2:sl) = (s2:s1:sl) : map (s1:) (transpositions (s2:sl))


-- 1.3 Lists
f :: [Int] -> Int
f xs = head ([0..] \\ xs)
\section{Definition of module: Matrices}

\subsection{Syntax required to define a module}
We also export function $rmt'$ for testing.
\begin{code}
module Matrices (rmvp, rmmp, rmt, rmt') where
type Realvector = [Double]
type Realmatrix = [[Double]]
\end{code}

\subsection{Matrix $\times$ Vector}
To multiply a matrix(M) with a vector(V), we can multiply $i^{th}$ column vector in $M$ 
with $i^{th}$ element in $V$ which would return a matrix with the same size as $M$. Sum
of each row of the returned matrix is the actual result. I use {\bf zipWith (+)} as well as
recursive to do this.

\begin{code}
rmvp :: Realmatrix -> Realvector -> Realvector
rmvp [[]] _ = []
rmvp _ [] = []
rmvp [] _ = []
rmvp (x:[]) (y:[]) = map (*y) x
rmvp (x:xs) (y:ys) = (zipWith (+)) (map (*y) x) (rmvp xs ys)
\end{code}

\subsection{Matrix $\times$ Matrix}
Quick easy if we know how to do the multiplication of matrix and vector. To calculate $M\times N$ where $N_i$ is the $i_{th}$ column vector of $N$, 
we will combine the result of $M\times N_i$ since $M\times N_i$ would return the $i^{th}$ column vector 
of the desired result.
\begin{code}
rmmp :: Realmatrix -> Realmatrix -> Realmatrix
rmmp _ [] = []
rmmp [] _ = []
rmmp x (y:ys) = (rmvp x y) : (rmmp x ys)
\end{code}

\subsection{Matrix transpose}
Recursively collect the head of each column vector will give us the correct result. We also 
need to take care of the edge condition since the matrix might be incomplete, such as 
$[[1,2], [2], [], [1,2,3]]$. I defined two function to do this using {\bf foldr} and {\bf map} 
separately.
\begin{code}
rmt :: Realmatrix -> Realmatrix
rmt [] = []
rmt x
  | new x == [] = []
  | otherwise =  foldr ((:).head) [] (new x) : rmt y
  where y = foldr ((:).tail) [] (new x)
\end{code}

\subsection{Matrix transpose, alternative}
Here is the alternative way.
\begin{code}
rmt' :: Realmatrix -> Realmatrix
rmt' [] = []
rmt' x 
  | new x == [] = []
  | otherwise = (map head (new x)) : rmt' (map tail (new x))
\end{code}

\subsection{Hellper function}
To drop empty list in a list. 
\begin{code}
new [] = []
new (x:xs) 
  | x == [] = new xs
  | otherwise = x : (new xs)
\end{code}

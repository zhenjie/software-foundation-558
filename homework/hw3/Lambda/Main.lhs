\documentclass[11pt]{article}
%% Literate Haskell script intended for lhs2TeX.

%include polycode.fmt
%format `union` = "\cup"
%format alpha = "\alpha"
%format gamma = "\gamma"
%format capGamma = "\Gamma"
%format tau = "\tau"
%format tau1 = "\tau_{1}"
%format tau2 = "\tau_{2}"
%format tau11 = "\tau_{11}"
%format tau12 = "\tau_{12}"
%format t12 = "t_{12}"
%format t1 = "t_{1}"
%format t1' = "t_{1}^{\prime}"
%format t2 = "t_{2}"
%format t2' = "t_{2}^{\prime}"
%format t3 = "t_{3}"
%format nv1 = "nv_{1}"

\usepackage{listings}
\lstset{escapeinside={\%*}{*)},keywordstyle=\color{blue},language=haskell, basicstyle={\scriptsize}}

\usepackage{xcolor}

\usepackage{fullpage}
\usepackage{mathpazo}
\usepackage{graphicx}
\usepackage{color}
\usepackage[centertags]{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{soul}



\usepackage{stmaryrd}

\title{Simply-typed call-by-value lambda-calculus with booleans and natural 
numbers\footnote{CS558: Homework 3}}
\author{Zhenjie Chen}
\date{\today}
\begin{document}
\maketitle
\thispagestyle{empty}

% \newpage

\section{Introduction}
In this exercise, we will implement a type checker and an evaluator based on the 
small-step evaluation relation for the simply-typed call-by-value lambda-calculus 
with booleans and numbers. The concrete syntax of {\bf Type} and {\bf Term} is:
\begin{verbatim}
Type --> arr lpar Type comma Type rpar
       | bool
       | nat

Term --> identifier
       | abs lpar identifier colon Type fullstop Term rpar
       | app lpar Term comma Term rpar
       | true
       | false
       | if Term then Term else Term fi
       | 0
       | succ lpar Term rpar
       | pred lpar Term rpar
       | iszero lpar Term rpar
       | lpar Term rpar
\end{verbatim}

\newpage

\section{Prologue}
\begin{code}
module Main where

import qualified System.Environment
import qualified Data.Map as M
import Data.List
import Data.Char(isAlpha, isAlphaNum)
import Text.Regex(subRegex, mkRegex)

class LatexShow tau where
  latexShow :: tau -> String
\end{code}

\section{Abstract-syntactic preliminaries}
\subsection{Types definition}
We need to add functional type, or more formally, type for abstraction.
\begin{code}
data Type  =  TypeArrow Type Type
           |  TypeBool
           |  TypeNat
           deriving Eq

instance Show Type where
  show  (TypeArrow tau1 tau2)   =  "->(" ++ show tau1 ++ "," ++ show tau2 ++ ")"
  show  TypeBool                =  "Bool"
  show  TypeNat                 =  "Nat"

instance LatexShow Type where
  latexShow  (TypeArrow tau1 tau2)    =  "$\\rightarrow$ (" ++ latexShow tau1
                                         ++ ", " ++ latexShow tau2 ++ ")"
  latexShow  TypeBool                 =  "Bool"
  latexShow  TypeNat                  =  "Nat"
\end{code}

\subsection{Terms definition}
In addition to what we have done in homework2, add three more terms' definition
for {\it Variable, Application, Abstraction}.
\begin{code}
type Var  =  String

data Term  =  Var Var
           |  Abs Var Type Term
           |  App Term Term
           |  Tru
           |  Fls
           |  If Term Term Term
           |  Zero
           |  Succ Term
           |  Pred Term
           |  IsZero Term
           deriving Eq

instance Show Term where
  show (Var x)        =  x
  show (Abs x tau t)  =  "abs(" ++ x ++ ":" ++ show tau ++ "." ++ show t ++ ")"
  show (App t1 t2)    =  "app(" ++ show t1  ++ "," ++ show t2 ++ ")"
  show Tru            =  "true"
  show Fls            =  "false"
  show (If t1 t2 t3)  =  "if " ++ show t1 ++ " then " ++ show t2 ++ " else " ++ show t3 ++ " fi"
  show Zero           =  "0"
  show (Succ t)       =  "succ(" ++ show t ++ ")"
  show (Pred t)       =  "pred(" ++ show t ++ ")"
  show (IsZero t)     =  "iszero(" ++ show t ++ ")"

instance LatexShow Term where
  latexShow  (Var x)            =  x
  latexShow  (Abs x tau t)      =  "$\\lambda$" ++ x ++ ": " ++ latexShow tau
                                   ++ ". " ++ latexShow t
  latexShow  (App t1 t2)        =  "$\\blacktriangleright$ (" ++ latexShow t1  ++ ", " 
                                   ++ latexShow t2 ++ ")"
  latexShow  Tru                =  "true"
  latexShow  Fls                =  "false"
  latexShow  (If t1 t2 t3)      =  "if " ++ latexShow t1 ++ " then " ++ latexShow t2
                                   ++ " else " ++ latexShow t3 ++ " fi"
  latexShow Zero                =  "0"
  latexShow (Succ t)            =  "succ(" ++ latexShow t ++ ")"
  latexShow (Pred t)            =  "pred(" ++ latexShow t ++ ")"
  latexShow (IsZero t)          =  "iszero(" ++ latexShow t ++ ")"
\end{code}

\newpage
\subsection{Binding and free variables}
We define function {\it fv} to collect all the free variables of a term.
\begin{code}
fv :: Term -> [Var]
fv (Var x)        =  [x]
fv (Abs x _ t1)   =  filter (/=x) (fv t1)
fv (App t1 t2) = fv t1 ++ fv t2
fv (If t1 t2 t3) = fv t1 ++ fv t2 ++ fv t3
fv (Succ t1) = fv t1
fv (Pred t1) = fv t1
fv (IsZero t1) = fv t1
fv _ = []
\end{code}

\subsection{Substitution}
Substitution: |subst x s t|, or $[x \mapsto s]t$ in Pierce,
is the result of substituting |s| for all the free occurrence of |x| in |t|.
This implementation of |subst| has been simplified for {\it Simply-typed 
call-by-value lambda-calculus with booleans and natural number}.

\begin{code}
subst :: Var -> Term -> Term -> Term
subst x v t =
      let fvars = fv t in 
         if not $ x `elem` fvars 
            then t
            else 
              case t of 
                    Var x -> v
                    Abs x' typeT1 t1 -> Abs x' typeT1 (subst x v t1)
                    App t1 t2 -> App (subst x v t1) (subst x v t2)
                    If t1 t2 t3 -> If (subst x v t1) (subst x v t2) (subst x v t3)
                    Succ t1 -> Succ (subst x v t1)
                    Pred t1 -> Pred (subst x v t1)
                    IsZero t1 -> IsZero (subst x v t1)
                    _ -> t
\end{code}

\newpage
\section{Small-step evaluation relation}
Here are some helper functions for Small-step evaluation:
\begin{code}
isValue :: Term -> Bool
isValue  (Abs _ _ _)  =  True
isValue  Tru          =  True
isValue  Fls          =  True
isValue  Zero         =  True
isValue  (Succ t)     =  isNumericValue t
isValue  _            =  False

isNumericValue :: Term -> Bool
isNumericValue  Zero      =  True
isNumericValue  (Succ t)  =  isNumericValue t
isNumericValue  _         =  False
\end{code}

Function |eval1| is one-step evaluation, ie. it takes a term and apply Small-step evaluation 
to this term, then return the evaluated term. It contains three phases: decompose term into 
context and possible redex, reduce, recompose. We extend the {\bf Arith evaluator} in 
homework 2 by adding patterns for application and abstraction. Here comes the evaluator for 
{\bf Simply-typed call-by-value lambda-calculus with booleans and natural numbers}.
\begin{code}
eval1 :: Term -> Maybe Term
eval1 (If Tru t2 _) = Just t2
eval1 (If Fls _ t3) = Just t3
eval1 (If t1 t2 t3) =  
  case eval1 t1 of
    Nothing -> Nothing
    Just Tru -> Just t2
    Just Fls -> Just t3 
    Just t1' -> Just (If t1' t2 t3)
eval1 (Succ t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Succ t1)
eval1 (Pred Zero) = Just Zero
eval1 (Pred (Succ nv)) 
  | isNumericValue nv = Just nv
eval1 (Pred t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(Pred t1)
eval1 (IsZero Zero) = Just Tru
eval1 (IsZero (Succ nv))
  | isNumericValue nv = Just Fls
eval1 (IsZero t) = 
  case eval1 t of
    Nothing -> Nothing
    Just t1 -> Just(IsZero t1)
eval1 (App t1 t2) =
  case eval1 t1 of
       Nothing -> 
           case eval1 t2 of
               Nothing -> 
                   case t1 of 
                        (Abs x typeT11 t12) -> Just (subst x t2 t12)
                        _ -> error "Expecte abstraction"
               Just t2' -> eval1 (App t1 t2')
       Just t1' -> eval1 (App t1' t2)
-- actually, the abstract is a value, does not need to be evaluated
eval1 term@(Abs x typeT11 t12) = 
  case eval1 t12 of
       -- cannot apply small step evaluation, it is a value
       Nothing -> Nothing
       -- not value
       Just t12' -> eval1 (Abs x typeT11 t12')
eval1 _ = Nothing
\end{code}

Here is the control function |eval| which recursively calls eval1 until reaching
the normal form if the term is well-typed.
\begin{code}
eval :: Term -> Term
eval t =
  case eval1 t of
    Just t' -> eval t'
    Nothing -> t
\end{code}

\newpage
\section{Typing}
\subsection{Typing context~($\Gamma$) definition}
We recursively define the typing context~($\Gamma$) to make it easier to look up a type in
$\Gamma$.
\begin{code}
data TypingContext  =  Empty
                    |  Bind TypingContext Var Type
                    deriving Eq
instance Show TypingContext where
  show Empty                  =  "<>"
  show (Bind capGamma x tau)  =  show capGamma ++ "," ++ x ++ ":" ++ show tau

contextLookup :: Var -> TypingContext -> Maybe Type
contextLookup x Empty  =  Nothing
contextLookup x (Bind capGamma y tau)
  | x == y      =  Just tau
  | otherwise   =  contextLookup x capGamma
\end{code}

\subsection{Type checking}
Following the typing rules, type the sub terms step by step and collect the basic types 
to build up the final type of the whole term.

\begin{code}
typing :: TypingContext -> Term -> Maybe Type
typing _ Tru = Just TypeBool
typing _ Fls = Just TypeBool
typing _ Zero = Just TypeNat
typing capGamma (Pred t) = 
       case typing capGamma t of
            Just TypeNat  -> Just TypeNat
            Nothing -> Nothing
typing capGamma (Succ t) = 
       case typing capGamma t of
            Just TypeNat -> Just TypeNat
            Nothing -> Nothing
typing capGamma (IsZero t) =
       case typing capGamma t of
            Just TypeNat -> Just TypeBool
            Nothing -> Nothing
typing capGamma term@(If tc t1 t2) = 
       case typing capGamma tc of 
            Nothing -> Nothing
            Just TypeBool -> 
                 case typing capGamma t1 of
                      Nothing -> Nothing
                      Just typeT1 -> 
                           case typing capGamma t2 of
                                Nothing -> Nothing
                                Just typeT2 -> 
                                     if typeT1 == typeT2 
                                        then return typeT1
                                        else error $ "Expected type: " ++ show typeT1 ++ " for \n'" 
                                             ++ show t2 ++ "'\nin \n" ++ show term ++ "\nwith actual type: " ++ show typeT2
            _ -> error "Expected Boolean type for 'if'"
typing capGamma (Abs x typeT1 t2) =
       case typing (Bind capGamma x typeT1) t2 of
            Nothing -> Nothing
            Just typeT2 -> Just (TypeArrow typeT1 typeT2)       
typing capGamma term@(App t1 t2) = 
       case typing capGamma t1 of
            Nothing -> Nothing
            Just (TypeArrow typeT1 typeT2) -> 
                 case typing capGamma t2 of
                      Nothing -> Nothing
                      Just typeT3 ->
                           if typeT1 == typeT3
                              then return typeT2
                              else error $ "Expected type: " ++ show typeT1 ++ " for \n'" ++ show t2 
                                   ++ "'\nin \n" ++ show term ++ "\nwith actual type: " ++ show typeT3
typing capGamma (Var x) = contextLookup x capGamma
\end{code}

Again, here is the control code for type checking:
\begin{code}
typeCheck :: Term -> Type
typeCheck t =
  case typing Empty t of
    Just tau -> tau
    _ -> error "type error"
\end{code}

\newpage
\section{Scanning and parsing}

\subsection{Token definition}
Most of the |Token| definition is the same as in homework 2. we will use |Identifier String|
to denote the variables in lambda calculus.
\begin{code}
data Token  =  Identifier String
            |  AbsKeyword
            |  AppKeyword
            |  TrueKeyword
            |  FalseKeyword
            |  IfKeyword
            |  ThenKeyword
            |  ElseKeyword
            |  FiKeyword
            |  BoolKeyword
            |  ZeroKeyword
            |  SuccKeyword
            |  PredKeyword
            |  IsZeroKeyword
            |  NatKeyword
            |  LPar
            |  RPar
            |  Comma
            |  Colon
            |  FullStop
            |  RightArrow
            deriving (Eq, Show)
\end{code}

\subsection{Scanner}
We still use |lex| function to scan the source code and generate a list of tokens. The 
only new and funny stuff is to judge the input as an legal identifier. Function |verifyId|
will serve for that.
\begin{code}
scan :: String -> [Token]
scan s =
  case lex s of
    [("", _)] -> []
    [(token, tl)] 
      | token `elem` keywordsList -> hash  M.! token : scan tl
      | verifyId token -> Identifier token : scan tl
      | otherwise -> error  $ "Invalid token: " ++ token 
      where 
        keywordsList = 
          ["true", "false", "if", "then", "else", "fi", "0", "succ", "(", 
          ")", "pred", "iszero", "abs", "app", ":", ".", "Nat", "Bool",
           ",", "->"]
        tokenList = 
          [TrueKeyword, FalseKeyword, IfKeyword, ThenKeyword, ElseKeyword, 
           FiKeyword, ZeroKeyword, SuccKeyword, LPar, RPar, PredKeyword, 
           IsZeroKeyword, AbsKeyword, AppKeyword, Colon, FullStop, 
           NatKeyword, BoolKeyword, Comma, RightArrow]
        hash = M.fromList $ zip keywordsList tokenList

verifyId :: [Char] -> Bool
verifyId [] = False
verifyId id@(x:xs) = and $ isAlpha x : map (isAlphaNum) id
\end{code}


\subsection{Parser}
Since we introduce types in the new term, we need to parse the type when there's any
abstraction in source code. |parseType| will do it for us. The others are almost the 
same as what we have done in homework2.
\begin{code}
parseType :: [Token] -> Maybe (Type, [Token])
parseType (RightArrow:LPar:tl) =
  case parseType tl of
    Just (tau1, Comma:tl') ->
      case parseType tl' of
        Just (tau2, RPar:tl'') -> Just (TypeArrow tau1 tau2, tl'')
        _ -> Nothing
    _ -> Nothing
parseType (BoolKeyword:tl) = Just (TypeBool, tl)
parseType (NatKeyword:tl) = Just (TypeNat, tl)
parseType _ = error "Syntax error in type declaration"

parseTerm :: [Token] -> Maybe (Term, [Token])
parseTerm (TrueKeyword:tl) = Just (Tru, tl)
parseTerm (FalseKeyword:tl) = Just (Fls, tl)
parseTerm (ZeroKeyword:tl) = Just(Zero, tl)
parseTerm (PredKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Pred t, tl1)
    _ -> error "Syntax error after 'pred'"
parseTerm (SuccKeyword:LPar:tl) =
  case parseTerm tl of 
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(Succ t, tl1)
    _ -> error "Syntax error after 'succ'"
parseTerm (IfKeyword:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t1, ThenKeyword:tl1) -> 
      case parseTerm tl1 of
        Nothing -> Nothing
        Just(t2, ElseKeyword:tl2) ->
          case parseTerm tl2 of
            Nothing -> Nothing
            Just(t3, FiKeyword:tl3) -> Just(If t1 t2 t3, tl3)
            _ -> error "Syntax error after 'else'"
    _ -> error "Syntax error after 'if'"
parseTerm (IsZeroKeyword:LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just(IsZero t, tl1)
    _ -> error "Syntax error after 'iszero'"
parseTerm (LPar:tl) =
  case parseTerm tl of
    Nothing -> Nothing
    Just(t, RPar:tl1) -> Just (t, tl1)
    _ -> error "Mismatching '(' and ')'"
parseTerm (Identifier s:tl) = Just (Var s, tl)
parseTerm (AbsKeyword:LPar:tl) =
  case parseTerm tl of
       Nothing -> Nothing
       Just (Var s, Colon:tl1) -> 
            case parseType tl1 of
                 Nothing -> Nothing
                 Just (TypeArrow tau1 tau2, FullStop:tl3) ->
                      case parseTerm tl3 of 
                           Nothing -> Nothing
                           Just(t1, RPar:tl3') -> Just(Abs s (TypeArrow tau1 tau2) t1, tl3')
                 Just(valueType, FullStop:tl2) -> 
                      case parseTerm tl2 of
                           Nothing -> Nothing
                           Just(t1, RPar:tl2') -> Just(Abs s valueType t1, tl2')
parseTerm (AppKeyword:LPar:tl) = 
  case parseTerm tl of
       Nothing -> Nothing
       Just(t1, Comma:tl1) ->
           case parseTerm tl1 of
                Nothing -> Nothing
                Just(t2, RPar:tl2) -> Just(App t1 t2, tl2)
parseTerm _ = Nothing


parse :: [Token] -> Term
parse ts =
  case parseTerm ts of
    Just (t, []) -> t
    Just (t, _) -> error "syntax error: spurious input at end"
    Nothing -> error "syntax error"
\end{code}

\newpage
\section{Epilogue}
\subsection{Main control flow}
\begin{code}
main :: IO ()
main =
    do
      args <- System.Environment.getArgs
      let [sourceFile] = args
      source <- readFile sourceFile
      let tokens = scan source
      let term = parse tokens
      putStrLn ("---Term:---")
      putStrLn (show term)
      --putStrLn (latexShow term)
      putStrLn ("---Type:---")
      putStrLn (show (typeCheck term))
      --putStrLn (latexShow (typeCheck term))
      putStrLn ("---Normal form:---")
      putStrLn (show (eval term))
      --putStrLn (latexShow (eval term))
\end{code}

\subsection{Multi-terms}
The same as in homework 2, we add support for multi terms terminated by ';', this
can be found in file {\it Main-multi.lhs}.

\subsection{Compile}
Refert to Makefile for more information,
\begin{enumerate}
\item {\it make tlbn} will build the executable file {\bf tlbn};
\item {\it make mtlbn} will build the executable file {\bf mtlbn} for multi-terms' testing;
\item {\it make doc} will build the document {\bf homework3.pdf}
\item {\it make all} will build the executable file {\bf tlbn}, document {\bf homework3.pdf} and open it with {\bf READER};
\item {\it make clean} clean  the project.
\end{enumerate}

\subsection{Testing}
After compiling the code with |make|, it is convenient to run the test using the 
simple testing script |runtest|. {\it ./runtest arg1 arg2} would run test for 
{\it ../Testcase/example\{arg1,arg2\}.TLBN}. {\it ./runtest} would run all the 
testcases.

\subsection{Test log}
Here is the log for running {\it ./runtest}, including all testcases.

\begin{lstlisting}
***************
* Testing 1 ...
***************
---Term:---
app(abs(x:Nat.succ(succ(x))),succ(0))
---Type:---
Nat
---Normal form:---
succ(succ(succ(0)))

***************
* Testing 2 ...
***************
---Term:---
if iszero(0) then succ(0) else false fi
---Type:---
tlbn: Expected type: Nat for 
'false'
in 
if iszero(0) then succ(0) else false fi
with actual type: Bool

***************
* Testing 3 ...
***************
---Term:---
app(abs(x:Bool.x),false)
---Type:---
Bool
---Normal form:---
false

***************
* Testing 4 ...
***************
---Term:---
app(app(abs(f:->(Nat,Bool).f),abs(x:Nat.iszero(x))),
        if iszero(0) then pred(0) else succ(0) fi)
---Type:---
Bool
---Normal form:---
true

***************
* Testing 5 ...
***************
---Term:---
abs(f:->(Nat,Bool).iszero(0))
---Type:---
->(->(Nat,Bool),Bool)
---Normal form:---
abs(f:->(Nat,Bool).iszero(0))

*************************
* Multi-terms testing ...
*************************
---Term:---
[app(abs(x:Nat.succ(succ(x))),succ(0)),
 if iszero(0) then succ(0) else pred(0) fi,
 app(abs(x:Bool.x),false),
 app(app(abs(f:->(Nat,Bool).f),abs(x:Nat.iszero(x))),
        if iszero(0) then pred(0) else succ(0) fi),
 abs(f:->(Nat,Bool).iszero(0))]
---Type:---
[Nat,Nat,Bool,Bool,->(->(Nat,Bool),Bool)]
---Normal form:---
[succ(succ(succ(0))),succ(0),false,true,abs(f:->(Nat,Bool).iszero(0))]

\end{lstlisting}

\end{document}

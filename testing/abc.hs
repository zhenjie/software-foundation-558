{-# LANGUAGE ScopedTypeVariables, TemplateHaskell #-}
module Main where

-- imports

import Test.QuickCheck
import Test.QuickCheck.All

rr xs = (reverse xs) == xs
     where types = xs::[Int]

--------------------------------------------------------------------------
-- main
main = $quickCheckAll
       
-- conj = printTestCase "Reverse of reverse" $(mono 'rr)

--------------------------------------------------------------------------
-- the end.

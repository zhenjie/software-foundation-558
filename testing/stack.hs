data Stack a = Stack [a] deriving (Show)

push :: Stack a -> a -> Stack a
push (Stack s) e = Stack (s ++ [e]) 

pop :: Stack a -> Stack a
pop (Stack []) = Stack []
pop (Stack s) = Stack $ (reverse . tail . reverse) s

pop1 :: Stack a -> Stack a
pop1 (Stack s) = Stack $ take (length s - 1) s


data Queue a = Queue [a] deriving (Show)

push' :: Queue a -> a -> Queue a
push' (Queue q) e = Queue (q ++ [e])

pop' :: Queue a -> Queue a
pop' (Queue []) = Queue []
pop' (Queue q) = Queue $ tail q


data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Eq)

(?+) :: Tree a -> a -> Tree a
(?+) EmptyTree  x = Node x EmptyTree EmptyTree
(?+) (Node x left right) y = (Node x (left ?+ y) right)

i :: (Eq a) => a -> Tree a -> Bool
i x EmptyTree = False
i x (Node r left right) = or [ r == x, x `i` left, x `i` right]
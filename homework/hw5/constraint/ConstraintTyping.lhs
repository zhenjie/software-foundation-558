\section{Constraints based type reconstruction}

\subsection{Reconstruction flow}
This subsection contains the data type definition and the control function 
for resconstructing a term based the type constraints. The general data flow
is: {\it term $\rightarrow$ type constraints set $\rightarrow$ encoded type constraints $\rightarrow$ unification result
    $\rightarrow$ decode the result as typesubstitution $\rightarrow$ reconstruct the term based on the
     typesubstitution}

\begin{code}
module ConstraintTyping where

import qualified AbstractSyntax as S
import qualified Unification as U
import qualified Data.List as D

type TypeConstraint = (S.Type, S.Type)
type TypeConstraintSet = [TypeConstraint]
type TypeSubstitution = [(S.TypeVar, S.Type)]

reconstructType :: S.Term -> Maybe S.Term
reconstructType t =
    let
        constraints = deriveTypeConstraints t
        unifencoding = encode constraints
        (unifoutcome, unifsolvedequations) = U.unify unifencoding
    in
        case unifoutcome of
            U.Success ->
              let
                typesubst = decode unifsolvedequations
                t' = applyTypeSubstitutionToTerm typesubst t
              in
                Just t'
            U.HaltWithFailure -> Nothing
            U.HaltWithCycle -> Nothing
            U.NoMatch -> Nothing

type TypeUnifVar = S.TypeVar
data TypeUnifFun = TypeUnifArrow | TypeUnifBool | TypeUnifNat
     deriving (Eq, Show)

encode :: TypeConstraintSet -> [U.Equation TypeUnifVar TypeUnifFun]
encode = map (\(tau1, tau2) -> (enctype tau1, enctype tau2))
  where
    enctype :: S.Type -> U.Term TypeUnifVar TypeUnifFun
    enctype (S.TypeArrow tau1 tau2) = U.Fun TypeUnifArrow [enctype tau1, enctype tau2]
    enctype S.TypeBool = U.Fun TypeUnifBool []
    enctype S.TypeNat = U.Fun TypeUnifNat []
    enctype (S.TypeVar xi) = U.Var xi

decode :: [U.Equation TypeUnifVar TypeUnifFun] -> TypeSubstitution
decode = map f
  where
    f :: (U.Term TypeUnifVar TypeUnifFun, U.Term TypeUnifVar TypeUnifFun)
        -> (S.TypeVar, S.Type)
    f (U.Var xi, t) = (xi, g t)
    g :: U.Term TypeUnifVar TypeUnifFun -> S.Type
    g (U.Fun TypeUnifArrow [t1, t2]) = S.TypeArrow (g t1) (g t2)
    g (U.Fun TypeUnifBool []) = S.TypeBool
    g (U.Fun TypeUnifNat []) = S.TypeNat
    g (U.Var xi) = S.TypeVar xi
\end{code}


\subsection{Deriving type constraints}
To make sure all the new type variables are fresh, we introduce 
an argument |State| to record how many type variables have been used when 
deriving the type constraints. |mkv| and |mkv'| are designed to make 
|S.Type| based on the current |State|. 

\begin{code}
mkv :: Int -> S.Type
mkv s = S.TypeVar $ "T" ++ show s

mkv' :: Int -> S.Type
mkv' s = S.TypeVar $ "X" ++ show s

type State = Int

deriveTypeConstraints1 :: S.TypingContext -> S.Term -> State -> Maybe (TypeConstraintSet, State, State)
deriveTypeConstraints1 ctc t s =
    case t of 
         S.Tru -> return ([(mkv s, S.TypeBool)], s, s+1)
         S.Fls -> return ([(mkv s, S.TypeBool)], s, s+1)
         S.Zero -> return ([(mkv s, S.TypeNat)], s, s+1)
         S.Pred t1 -> 
             case deriveTypeConstraints1 ctc t1 s of
                  Nothing -> Nothing
                  Just (tcs, cur, s1) -> return ([(mkv s1, S.TypeNat), (mkv cur, S.TypeNat)] ++ 
                                                tcs, s1, s1+1)
         S.Succ t1 -> 
             case deriveTypeConstraints1 ctc t1 s of
                  Nothing -> Nothing
                  Just (tcs, cur, s1) -> return ([(mkv s1, S.TypeNat), (mkv cur, S.TypeNat)] ++ 
                                                tcs, s1, s1+1)
         S.IsZero t1 -> 
             case deriveTypeConstraints1 ctc t1 s of
                  Nothing -> Nothing
                  Just (tcs, cur, s1) -> return ([(mkv cur, S.TypeNat), (mkv s1, S.TypeBool)] ++ tcs, s1, s1+1)

         S.Var x -> 
               case S.contextLookup x ctc of
                    Nothing -> Nothing 
                    Just typeX -> return ([(mkv s, typeX)], s, s+1)
         S.If t1 t2 t3 ->
              case deriveTypeConstraints1 ctc t1 s of 
                   Nothing -> Nothing
                   Just (tcs1, cur1, s1) -> 
                        case deriveTypeConstraints1 ctc t2 s1 of 
                             Nothing -> Nothing
                             Just (tcs2, cur2, s2) -> 
                                  case deriveTypeConstraints1 ctc t3 s2 of
                                       Nothing -> Nothing
                                       Just (tcs3, cur3, s3) -> 
                                            return ([(mkv cur1, S.TypeBool), 
                                                     (mkv cur2, mkv cur3),
                                                     (mkv s3, mkv cur2)]
                                                     ++ tcs1 ++ tcs2 ++ tcs3, s3, s3+1)
         S.AbsType x typeT1 t2 ->
               case deriveTypeConstraints1 (S.Bind ctc x (mkv' s)) t2 (s+1) of
                    Nothing -> Nothing
                    Just (tcs, cur1, s1) -> return ([(mkv s1, S.TypeArrow (mkv' s) (mkv cur1)), (mkv' s, typeT1)] 
                                                   ++ tcs, s1, s1+1)
         S.Abs x t2 ->
               case deriveTypeConstraints1 (S.Bind ctc x (mkv' s)) t2 (s+1) of
                    Nothing -> Nothing
                    Just (tcs, cur1, s1) -> return ([(mkv s1, S.TypeArrow (mkv' s) (mkv cur1))] ++ tcs, s1, s1+1)
         S.App t1 t2 ->
               case deriveTypeConstraints1 ctc t1 s of
                    Nothing -> Nothing
                    Just (tcs1, cur1, s1) ->
                         case deriveTypeConstraints1 ctc t2 s1 of
                              Nothing -> Nothing
                              Just (tcs2, cur2, s2) ->
                                   return ( [(mkv cur1, S.TypeArrow (mkv cur2) (mkv' s2))] ++ tcs1 ++ tcs2, s2, s2+1)
         S.Fix t1 -> 
               case deriveTypeConstraints1 ctc t1 s of 
                    Nothing -> Nothing 
                    Just (tcs, cur1, s1) -> 
                         return ([(mkv cur1, S.TypeArrow (mkv' s1) (mkv' s1))] ++ tcs, s1, s1+1)
 
deriveTypeConstraints :: S.Term -> TypeConstraintSet
deriveTypeConstraints t = 
    case deriveTypeConstraints1 S.Empty t 1 of
         Nothing -> error "Error occurs when deriving type constraint set..."
         Just(tcs, _, _) -> tcs

\end{code}


\subsection{Resconstruct the term with type based on the unification result}
We need to find out all the variables and its corresponding type variables. 
As deriving the type constraints, however, we simply record the |State| and 
add correct type information for each variable based on the |State|.

\begin{code}
applyTypeSubstitutionToTerm1 :: TypeSubstitution -> State -> S.Term -> Maybe (S.Term, State)
applyTypeSubstitutionToTerm1 ts s t =
    case t of 
         S.Tru -> return (t, s+1)
         S.Fls -> return (t, s+1)
         S.Zero -> return (t, s+1)
         S.Pred t1 ->
                case applyTypeSubstitutionToTerm1 ts s t1 of
                     Nothing -> Nothing
                     Just (t1', s1) -> return (S.Pred t1', s1+1)
         S.Succ t1 ->
                case applyTypeSubstitutionToTerm1 ts s t1 of
                     Nothing -> Nothing
                     Just (t1', s1) -> return (S.Succ t1', s1+1)
         S.IsZero t1 ->
                case applyTypeSubstitutionToTerm1 ts s t1 of
                     Nothing -> Nothing
                     Just (t1', s1) -> return (S.IsZero t1', s1+1)
         S.Var x -> return (S.Var x, s+1)
         S.If t1 t2 t3 ->
              case applyTypeSubstitutionToTerm1 ts s t1 of
                  Nothing -> Nothing
                  Just (t1', s1) -> 
                      case applyTypeSubstitutionToTerm1 ts s1 t2 of
                          Nothing -> Nothing
                          Just (t2', s2) ->
                               case applyTypeSubstitutionToTerm1 ts s2 t3 of
                                   Nothing -> Nothing
                                   Just (t3', s3) -> return (S.If t1' t2' t3', s3+1)
         S.AbsType x typeT1 t2 -> 
             case applyTypeSubstitutionToTerm1 ts (s+1) t2 of
                 Nothing -> Nothing
                 Just (t2', s1) -> return (S.AbsType x typeT1 t2', s1+1)
         S.Abs x t2 ->
             case applyTypeSubstitutionToTerm1 ts (s+1) t2 of
                 Nothing -> Nothing
                 Just (t2', s1) -> return (S.AbsType x (typeLookup ts s) t2', s1+1)
                      where 
                            typeLookup :: TypeSubstitution -> State -> S.Type
                            typeLookup ts s = 
                                case D.find (\(typeVar1, typeT2) -> typeVar1 == ("X" ++ show s)) ts of
                                    Just (typeV1, type2) -> type2
                                    Nothing -> mkv' s
         S.App t1 t2 -> 
             case applyTypeSubstitutionToTerm1 ts s t1 of
                 Nothing -> Nothing
                 Just (t1', s1) -> 
                      case applyTypeSubstitutionToTerm1 ts s1 t2 of
                           Nothing -> Nothing
                           Just (t2', s2) -> return (S.App t1' t2', s2+1)
         S.Fix t1 -> 
               case applyTypeSubstitutionToTerm1 ts s t1 of
                    Nothing -> Nothing
                    Just (t1', s1) -> return (S.Fix t1', s1+1)

applyTypeSubstitutionToTerm :: TypeSubstitution -> S.Term-> S.Term
applyTypeSubstitutionToTerm ts t = 
    case applyTypeSubstitutionToTerm1 ts 1 t of
         Just (t', _) -> t'
         Nothing -> error "Error occurs when applying type substitution to term"

\end{code}
